<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'MainController@main');
//Route::get('{path}', 'MainController@main')->where('path', '[а-яА-ЯЁёa-zA-Z0-9\-/_\[\].┼♪♫; ]+');

//$users = \App\Models\User::orderByRaw('FIELD(name, "sofiya","Пар.","Nurs") desc')->orderBy("created_at","desc")->get();
//dd($users);



Route::get('/1c/user', 'MainController@user_main');

Route::get('/resize', 'MainController@resize');
Route::get('/admin', 'MainController@admin_main');
Route::get('/admin/{path}', 'MainController@admin_main')->where('path', '[а-яА-ЯЁёa-zA-Z0-9\-/_\[\].┼♪♫; ]+');


Route::get('/', 'MainController@main');
Route::get('/{path}', 'MainController@main')->where('path', '[а-яА-ЯЁёa-zA-Z0-9\-/_\[\].┼♪♫; ]+');


