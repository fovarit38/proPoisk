<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Route::post('/ferialfile', 'Api\Admin\MainController@ferialfile');
//Route::post('/checkgile', 'Api\Admin\MainController@checkgile');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/card/add', 'Api\Admin\MainController@card_add');

Route::post('/update/files', 'Api\Admin\MainController@update_files');
Route::get('/favorit/add/{id}', 'Api\Client\MainController@favoritSave');

Route::get('/ru/offer', function(){
return '
asd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asd
asd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asd
asd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asd
asd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asd
asd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asdasd asd

';
});

Route::get('/get/filters', 'Api\Client\MainController@get_filters');
Route::get('/get/filters/{id}', 'Api\Client\MainController@get_filters_one');

Route::get('/favorit/remove/{id}', 'Api\Client\MainController@favoritRemove');
Route::get('/favorit/list', 'Api\Client\MainController@favoritList');
Route::get('/favorit/list/product', 'Api\Client\MainController@favoritListProduct');

Route::get('success', 'Api\Admin\MainController@order_success');
Route::get('error', 'Api\Admin\MainController@order_error');


Route::get('hall/{id}', 'MainController@hall');
Route::get('/inputs', 'MainController@inputs');

Route::get('/top/list', 'Api\Client\MainController@top_list');
Route::get('/top/main', 'Api\Client\MainController@main_list');
Route::get('/maps', 'Api\Client\MainController@maps');




Route::get('/menu/search', 'Api\Admin\MainController@menu_search');


Route::post('/reserved', 'Api\Admin\MainController@reserved');
Route::post('/reserved/save', 'Api\Admin\MainController@reserved_save');

Route::get('/crest/install', 'MainController@installApp');
Route::post('/crest/install', 'MainController@installAppPost');


Route::post('/profile/save', 'Api\Admin\MainController@profile_save');

Route::post('/feedback/save', 'Api\Admin\MainController@feedback_save');


Route::post('/admin/delete_point', 'Api\Admin\MainController@delete_point');
Route::post('/admin/add_point', 'Api\Admin\MainController@add_point');
Route::post('/admin/update_point', 'Api\Admin\MainController@update_point');
Route::post('/admin/update_size', 'Api\Admin\MainController@update_size');

Route::get('/admin/get_point/{id}', 'Api\Admin\MainController@get_point');

Route::get('/auth/user', 'Api\AuthController@get_user');
Route::post('/auth/login', 'Api\AuthController@auth_login');

Route::get('/auth/mobile/login', 'Api\AuthController@auth_login_mobile');
Route::get('/auth/mobile/register', 'Api\AuthController@register');

Route::get('/admin/type_row_db', 'Api\Admin\MainController@typeRowDb');


Route::get('/user/update', 'Api\Admin\MainController@user_update');
Route::get('/message/send', 'Api\Admin\MainController@user_update');


Route::get('/order/list', 'Api\Admin\MainController@order_list');

Route::post('/order/pay', 'Api\Admin\MainController@order_pay');
Route::post('/order/save', 'Api\Admin\MainController@order_save');
Route::post('/status/update', 'Api\Admin\MainController@order_status');

Route::group(['prefix' => '/table/'], function () {
    Route::post('/update', 'Api\Admin\Constructor\TableController@update');
    Route::post('/delete', 'Api\Admin\Constructor\TableController@delete');
    Route::get('/list', 'Api\Admin\Constructor\TableController@table_list');
    Route::get('/list/{model}', 'Api\Admin\Constructor\TableController@table_list_model');
});

Route::group(['prefix' => '/database'], function () {
    Route::group(['prefix' => '/bread/{model}'], function () {
        Route::get('/get', 'Api\Admin\Constructor\BreadController@database_bread_get');
        Route::post('/update', 'Api\Admin\Constructor\BreadController@database_bread_update');
    });
    Route::group(['prefix' => '/{model}'], function () {
        Route::get('/browse_column', 'Api\Admin\Database\MainController@browse_column_get');
        Route::post('/delete', 'Api\Admin\Database\MainController@browse_delete');
        Route::get('/edit_column/{id}', 'Api\Admin\Database\MainController@edit_column_get');
    });
    Route::post('/update', 'Api\Admin\Database\MainController@database_update');
    Route::get('/model_list', 'Api\Admin\Database\MainController@database_list');
    Route::get('/get/{model}/{id}', 'Api\Admin\Database\MainController@database_single_get');
    Route::get('/get/{model}', 'Api\Admin\Database\MainController@database_all_get');
});
