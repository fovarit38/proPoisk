const mix = require('laravel-mix');
let smartgrid = require('smart-grid');
const CopyPlugin = require('copy-webpack-plugin');

var settings = {
    outputStyle: 'scss',
    columns: 12,
    offset: "0px",
    container: {
        maxWidth: '1200px',
        fields: '0px'
    },
    breakPoints: {

        lg: {
            'width': '2400px',
            'fields': '0px'
        },

        nb: {
            'width': '1475px',
            'fields': '0px'
        },
        w1440: {
            'width': '1490px',
            'fields': '0px'
        },
        w1400: {
            'width': '1400px',
            'fields': '0px'
        },
        w1420: {
            'width': '1490px',
            'fields': '0px'
        },
        w1360: {
            'width': '1360px',
            'fields': '0px'
        },
        w1280: {
            'width': '1280px',
            'fields': '0px'
        },
        w1230: {
            'width': '1230px',
            'fields': '0px'
        },
        w1180: {
            'width': '1180px',
            'fields': '0px'
        },
        w1100: {
            'width': '1100px',
            'fields': '0px'
        },
        md: {
            'width': '1030px',
            'fields': '0px'
        },

        w1015: {
            'width': '1015px',
            'fields': '0px'
        },
        w950: {
            'width': '950px',
            'fields': '0px'
        },
        sm: {
            'width': '780px',
            'fields': '0px'
        },
        xs: {
            'width': '640px',
            'fields': '0px'
        },
        xs2: {
            'width': '380px',
            'fields': '0px'
        }

    }
};

smartgrid('./resources/assets/overall/sass/', settings);


var
    dirdata = __dirname;
dirdata = dirdata.split('\\');
dirdata = (dirdata[dirdata.length - 1]);


var procsi = ['client','admin'];

procsi.forEach(function (value, index) {

    mix.setPublicPath('public/media/' + value + '/');



});


