<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="{{ asset('public/media/admin/js/app.js') }}?v=25" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons"
          rel="stylesheet">
    {{--    https://material.io/resources/icons/?style=baseline--}}
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('public/media/admin/css/app.css') }}?v=9" rel="stylesheet">
</head>
<body>

@yield('content')

<script>
    window.laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => Auth::user(),
            'api_token' => (Auth::user()) ? Auth::user()->api_token : null
        ]) !!};
</script>

</body>
</html>
