@extends('client.layouts.app')

@section('content')

    @php
        $jsens= \App\Models\Restaurant::get()->toarray();
    @endphp
    <div id="maps" data-info="{{json_encode($jsens)}}"></div>

    <style>
        body, html, #maps {
            display: flex;
            flex-direction: column;
            width: 100%;
            height: 100%;
        }

        .inputs {
            display: flex;
            justify-content: center;
            padding-top: 7px;
        }

        input[type="date"]::-webkit-inner-spin-button,
        input[type="date"]::-webkit-calendar-picker-indicator {
            display: none;
            -webkit-appearance: none;
        }

        #constructor {
            width: 100vw;
            height: 100vh;
            background-repeat: no-repeat;
            background-position: 0 0;
            margin-top: 1rem;
        }

        .input + .input {
            margin-left: 20px;
        }

        .input {
            width: 40%;
            height: 50px;
            border: 1px solid #2B3080;
            box-sizing: border-box;
            border-radius: 25px;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: flex-start;
            position: relative;

        }

        .btn-zoom {
            width: 42px;
            height: 42px;
            border-radius: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .2);
            background-color: #fff;
            padding: 0;
        }

        .btn-zoom + .btn-zoom {
            margin-top: 1rem;

        }

        .input input {
            width: calc(100% - 12px);
            height: 100%;
            background: none;
            border: none;
            position: absolute;
            left: 0;
            top: 0;
            font-family: 'Open Sans', sans-serif;
            font-style: normal;
            font-weight: normal;
            font-size: 12px;
            line-height: 16px;
            display: flex;
            align-items: center;
            color: #2B3080;
            padding-left: 24px;
            box-sizing: border-box;
            margin-right: 27px;
            outline: 0;

        }

        .nav-bar {
            position: absolute;
            right: 1rem;
            top: 1rem;
            display: flex;
            flex-direction: column;
        }


        .b_centers {
            position: relative;
            z-index: 10;
        }

        label {
            font-family: 'Open Sans', sans-serif;
            font-style: normal;
            font-weight: 600;
            font-size: 12px;
            line-height: 16px;
            display: flex;
            align-items: center;
            color: #2B3080;
            z-index: 40;
            position: absolute;
            left: 22px;
            transform: translateY(-50%);
            top: 0;
            background-color: #fff;
            padding: 0 5px;
        }
    </style>

@endsection
