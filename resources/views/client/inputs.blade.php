@extends('client.layouts.app')

@section('content')

<link rel="preconnect" href="https://fonts.gstatic.com">
<link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
        rel="stylesheet">

@php
$city_name=isset($_REQUEST["city"])?$_REQUEST["city"]:"";
$city_date=isset($_REQUEST["date"])?$_REQUEST["date"]:"";

$date_info=date('Y-m-d');
if($city_date!=""){
$date_info=date('Y-m-d',strtotime($city_date));
}

$val = file_get_contents("https://teamgis.a-lux.dev/public/geto?ip=".$_SERVER['REMOTE_ADDR']);
$cityx="2";
if($val=="Алматы"){
$cityx="1";
}
@endphp
<div class="inputs">
    <div class="input">
        <label for="">Город</label>
        <select
                style=" width: calc(100% - 12px); height: 100%; background: none; border: none; position: absolute; left: 0; top: 0; font-family: 'Open Sans', sans-serif; font-style: normal; font-weight: normal; font-size: 12px; line-height: 16px; display: flex; align-items: center; color: #636363; padding-left: 24px; box-sizing: border-box; margin-right: 27px; outline: 0; "
                id="city" class="date_box city_ck" type="date">
            @foreach(\App\Models\City::get() as $city)
            <option <?=($cityx==$city->id?"selected":"")?>  @if($city_name==$city->id) selected
                @endif value="{{$city->id}},{{$city->latitude}},{{$city->longitude}}">{{$city->name}}
            </option>
            @endforeach
        </select>
    </div>
    <div class="input">
        <label for="">Дата</label>
        <input id="date" class="date_box date_ck" type="date"
               style="background-image: url('/public/media/client/images/notification/event_note_24px.png');background-size: 18px;background-repeat: no-repeat;background-position: right 8px center;"
               value="<?php echo $date_info; ?>">
    </div>
</div>


<style>

    .inputs {
        display: flex;
        justify-content: center;
        padding-top: 7px;
    }

    input[type="date"]::-webkit-inner-spin-button,
    input[type="date"]::-webkit-calendar-picker-indicator {
        display: none;
        -webkit-appearance: none;
    }

    #constructor {
        width: 100vw;
        height: 100vh;
        background-repeat: no-repeat;
        background-position: 0 0;
        margin-top: 1rem;
    }

    .input + .input {
        margin-left: 20px;
    }

    .input {
        width: 40%;
        height: 50px;
        border: 1px solid #2B3080;
        box-sizing: border-box;
        border-radius: 25px;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: flex-start;
        position: relative;

    }

    .btn-zoom {
        width: 42px;
        height: 42px;
        border-radius: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .2);
        background-color: #fff;
        padding: 0;
    }

    .btn-zoom + .btn-zoom {
        margin-top: 1rem;

    }

    .input input {
        width: calc(100% - 12px);
        height: 100%;
        background: none;
        border: none;
        position: absolute;
        left: 0;
        top: 0;
        font-family: 'Open Sans', sans-serif;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 16px;
        display: flex;
        align-items: center;
        color: #2B3080;
        padding-left: 24px;
        box-sizing: border-box;
        margin-right: 27px;
        outline: 0;

    }

    .nav-bar {
        position: absolute;
        right: 1rem;
        top: 1rem;
        display: flex;
        flex-direction: column;
    }


    .b_centers {
        position: relative;
        z-index: 10;
    }

    label {
        font-family: 'Open Sans', sans-serif;
        font-style: normal;
        font-weight: 600;
        font-size: 12px;
        line-height: 16px;
        display: flex;
        align-items: center;
        color: #2B3080;
        z-index: 40;
        position: absolute;
        left: 22px;
        transform: translateY(-50%);
        top: 0;
        background-color: #fff;
        padding: 0 5px;
    }
</style>

@endsection
