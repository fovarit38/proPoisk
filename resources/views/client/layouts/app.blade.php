<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=a2a8abd6-66b5-4670-8e3a-4df11e81e634" type="text/javascript"></script>
    <!-- Scripts -->


    <script src="{{ asset('public/media/client/js/app.js') }}?v=87" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('public/media/client/css/app.css') }}?v=15" rel="stylesheet">
</head>
<body>
    @yield('content')
</body>
</html>
