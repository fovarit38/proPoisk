@extends('client.layouts.app')

@section('content')

    @php
        $date_info=date('Y-m-d');
     $city_date=isset($_REQUEST["date"])?$_REQUEST["date"]:"";


        if($city_date!=""){
          $date_info=date('Y-m-d',strtotime($city_date));
        }

       $time_info=date('H:i');
         $city_date=isset($_REQUEST["time"])?$_REQUEST["time"]:"";
        if($city_date!=""){
          $time_info=date('H:i',strtotime($city_date));
        }

    @endphp
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
        rel="stylesheet">

    <div class="inputs">

        <div class="input">
            <label for="">Дата</label>
            <input id="date"
                   style="background-image: url('/public/media/client/images/notification/event_note_24px.png');background-size: 18px;background-repeat: no-repeat;background-position: right 8px center;"
                   class="date_box" type="date" value="<?php echo $date_info; ?>">
        </div>
        <div class="input">
            <label for="">Время</label>
            <input id="time"
                   style="background-image: url('/public/media/client/images/notification/access_time_24px.png');background-size: 18px;background-repeat: no-repeat;background-position: right 8px center;"
                   class="date_box" type="time" value="<?php echo $time_info; ?>">
        </div>
    </div>


    <div class="b_centers">
        <canvas id="constructor" data-hall="{{json_encode($hall)}}"></canvas>

        {{--        <div class="nav-bar">--}}
        {{--            <a href="javascript:void(null);" class="btn btn-zoom btn-zoom-plus">--}}
        {{--                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32"--}}
        {{--                     fill="currentColor">--}}
        {{--                    <path d="M24 15h-7V8h-2v7H8v2h7v7h2v-7h7v-2z"></path>--}}
        {{--                </svg>--}}
        {{--            </a>--}}
        {{--            <a href="javascript:void(null);" class="btn btn-zoom btn-zoom-minus">--}}
        {{--                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" fill="currentColor"--}}
        {{--                     width="32"--}}
        {{--                     height="32">--}}
        {{--                    <path d="M8 15h16v2H8z"></path>--}}
        {{--                </svg>--}}
        {{--            </a>--}}
        {{--        </div>--}}
    </div>
    <style>

        .inputs {
            display: flex;
            justify-content: center;
            padding-top: 24px;
        }

        input[type="date"]::-webkit-inner-spin-button,
        input[type="date"]::-webkit-calendar-picker-indicator {
            display: none;
            -webkit-appearance: none;
        }

        input[type="time"]::-webkit-inner-spin-button,
        input[type="time"]::-webkit-calendar-picker-indicator {
            display: none;
            -webkit-appearance: none;
        }

        #constructor {
            width: 100vw;
            height: 100vh;
            background-repeat: no-repeat;
            background-position: 0 0;
            margin-top: 1rem;
        }

        .input + .input {
            margin-left: 20px;
        }

        .input {
            width: 40%;
            height: 50px;
            border: 1px solid #2B3080;
            box-sizing: border-box;
            border-radius: 25px;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: flex-start;
            position: relative;

        }

        .btn-zoom {
            width: 42px;
            height: 42px;
            border-radius: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .2);
            background-color: #fff;
            padding: 0;
        }

        .btn-zoom + .btn-zoom {
            margin-top: 1rem;

        }

        .input input {
            width: calc(100% - 12px);
            height: 100%;
            background: none;
            border: none;
            position: absolute;
            left: 0;
            top: 0;
            font-family: 'Open Sans', sans-serif;
            font-style: normal;
            font-weight: normal;
            font-size: 12px;
            line-height: 16px;
            display: flex;
            align-items: center;
            color: #2B3080;
            padding-left: 24px;
            box-sizing: border-box;
            margin-right: 27px;
            outline: 0;

        }

        .nav-bar {
            position: absolute;
            right: 1rem;
            top: 1rem;
            display: flex;
            flex-direction: column;
        }


        .b_centers {
            position: relative;
            z-index: 10;
        }

        label {
            font-family: 'Open Sans', sans-serif;
            font-style: normal;
            font-weight: 600;
            font-size: 12px;
            line-height: 16px;
            display: flex;
            align-items: center;
            color: #2B3080;
            z-index: 40;
            position: absolute;
            left: 22px;
            transform: translateY(-50%);
            top: 0;
            background-color: #fff;
            padding: 0 5px;
        }

        .canvas-container {
            background: none !important;
        }
    </style>

@endsection
