import * as PIXI from 'pixi.js';
import jquery from 'jquery';

window.$ = window.jQuery = jquery;

import {init, mapinit, addPoint, addPointCustom} from './mapsMy.js';
import {fabric} from "fabric";

var zoom_controls = null;


(function ($) {
    var CURRENT_TOUCH, FIRST_TOUCH, GESTURE, GESTURES, HOLD_DELAY, TOUCH_TIMEOUT, _angle, _capturePinch,
        _captureRotation, _cleanGesture, _distance, _fingersPosition, _getTouches, _hold, _isSwipe, _listenTouches,
        _onTouchEnd, _onTouchMove, _onTouchStart, _parentIfText, _swipeDirection, _trigger, _current;

    _current = null;
    GESTURE = {};
    FIRST_TOUCH = [];
    CURRENT_TOUCH = [];
    TOUCH_TIMEOUT = void 0;
    HOLD_DELAY = 650;
    GESTURES = ["singleTap", "doubleTap", "hold", "swipe", "swiping", "swipeLeft", "swipeRight", "swipeUp", "swipeDown", "rotate", "rotating", "rotateLeft", "rotateRight", "pinch", "pinching", "pinchIn", "pinchOut", "drag", "dragLeft", "dragRight", "dragUp", "dragDown"];

    $.each({touch: "touchstart", tap: "tap"}, function (event, shortcut) {
        $.fn[event] = function (callback) {
            return $(document.body).on(shortcut, this, callback);
        };
    });

    $.each(GESTURES, function (k, event) {
        $.fn[event] = function (callback) {
            return this.on(event, this.selector, callback);
        };
    });

    $(document).ready(function () {
        return _listenTouches();
    });

    _listenTouches = function () {
        return $(document.body)
            .on("touchstart", _onTouchStart)
            .on("touchmove", _onTouchMove)
            .on("touchend", _onTouchEnd)
            .on("touchcancel", _cleanGesture);
    };

    _onTouchStart = function (event) {
        var delta, fingers, now, touches;
        now = Date.now();
        delta = now - (GESTURE.last || now);
        TOUCH_TIMEOUT && clearTimeout(TOUCH_TIMEOUT);
        touches = _getTouches(event);
        fingers = touches.length;
        FIRST_TOUCH = _fingersPosition(touches, fingers);
        GESTURE.el = $(_parentIfText(touches[0].target));
        GESTURE.fingers = fingers;
        GESTURE.last = now;
        if (fingers === 1) {
            GESTURE.isDoubleTap = delta > 0 && delta <= 250;
            return setTimeout(_hold, HOLD_DELAY);
        } else if (fingers === 2) {
            GESTURE.initial_angle = parseInt(_angle(FIRST_TOUCH), 10);
            GESTURE.initial_distance = parseInt(_distance(FIRST_TOUCH), 10);
            GESTURE.angle_difference = 0;
            return GESTURE.distance_difference = 0;
        }
    };

    _onTouchMove = function (event) {
        var fingers, touches;
        if (GESTURE.el) {
            touches = _getTouches(event);
            fingers = touches.length;
            if (fingers === GESTURE.fingers) {
                CURRENT_TOUCH = _fingersPosition(touches, fingers);
                if (_isSwipe(event)) {
                    _trigger("swiping");
                }
                if (fingers === 2) {
                    _captureRotation();
                    _capturePinch();
                    event.preventDefault();
                }
            } else {
                _cleanGesture();
            }
        }
        return true;
    };

    _isSwipe = function () {
        var move_horizontal, move_vertical, ret;
        ret = false;
        if (CURRENT_TOUCH[0]) {
            move_horizontal = Math.abs(FIRST_TOUCH[0].x - CURRENT_TOUCH[0].x) > 30;
            move_vertical = Math.abs(FIRST_TOUCH[0].y - CURRENT_TOUCH[0].y) > 30;
            ret = GESTURE.el && (move_horizontal || move_vertical);
        }
        return ret;
    };

    _onTouchEnd = function () {
        var anyevent, drag_direction, pinch_direction, rotation_direction, swipe_direction;
        if (GESTURE.fingers === 1) {
            if (_isSwipe()) {
                _trigger("swipe");
                swipe_direction = _swipeDirection(FIRST_TOUCH[0].x, CURRENT_TOUCH[0].x, FIRST_TOUCH[0].y, CURRENT_TOUCH[0].y);
                _trigger("swipe" + swipe_direction);
                return _cleanGesture();
            } else {
                if (GESTURE.isDoubleTap) {
                    _trigger("doubleTap");
                    return _cleanGesture();
                } else {
                    TOUCH_TIMEOUT = setTimeout(function () {
                        _trigger("singleTap");
                        _cleanGesture();
                    }, 250);
                    return TOUCH_TIMEOUT;
                }
            }
        } else if (GESTURE.fingers === 2) {
            anyevent = false;

            if (GESTURE.angle_difference !== 0) {
                _trigger("rotate", {
                    angle: GESTURE.angle_difference
                });
                rotation_direction = GESTURE.angle_difference > 0 ? "rotateRight" : "rotateLeft";
                _trigger(rotation_direction, {
                    angle: GESTURE.angle_difference
                });
                anyevent = true;
            }

            if (GESTURE.distance_difference !== 0) {
                _trigger("pinch", {
                    angle: GESTURE.distance_difference
                });
                pinch_direction = GESTURE.distance_difference > 0 ? "pinchOut" : "pinchIn";
                _trigger(pinch_direction, {
                    distance: GESTURE.distance_difference
                });
                anyevent = true;
            }

            if (!anyevent && CURRENT_TOUCH[0]) {
                if (Math.abs(FIRST_TOUCH[0].x - CURRENT_TOUCH[0].x) > 10 || Math.abs(FIRST_TOUCH[0].y - CURRENT_TOUCH[0].y) > 10) {
                    _trigger("drag");
                    drag_direction = _swipeDirection(FIRST_TOUCH[0].x, CURRENT_TOUCH[0].x, FIRST_TOUCH[0].y, CURRENT_TOUCH[0].y);
                    _trigger("drag" + drag_direction);
                }
            }
            return _cleanGesture();
        }
    };

    _fingersPosition = function (touches, fingers) {
        var result = [],
            i = 0;
        touches = touches[0].targetTouches ? touches[0].targetTouches : touches;
        while (i < fingers) {
            result.push({
                x: touches[i].pageX,
                y: touches[i].pageY
            });
            i++;
        }
        return result;
    };

    _captureRotation = function () {
        var angle, diff, i, symbol;
        angle = parseInt(_angle(CURRENT_TOUCH), 10);
        diff = parseInt(GESTURE.initial_angle - angle, 10);
        if (Math.abs(diff) > 20 || GESTURE.angle_difference !== 0) {
            i = 0;
            symbol = GESTURE.angle_difference < 0 ? "-" : "+";
            while (Math.abs(diff - GESTURE.angle_difference) > 90 && i++ < 10) {
                eval("diff " + symbol + "= 180;");
            }
            GESTURE.angle_difference = parseInt(diff, 10);
            return _trigger("rotating", {
                angle: GESTURE.angle_difference
            });
        }
    };

    _capturePinch = function () {
        var diff, distance;
        distance = parseInt(_distance(CURRENT_TOUCH), 10);
        diff = GESTURE.initial_distance - distance;
        if (Math.abs(diff) > 10) {
            GESTURE.distance_difference = diff;
            window.distance = diff;
            return _trigger("pinching", {
                distance: diff
            });
        }
    };

    _trigger = function (type, params) {
        if (GESTURE.el) {
            params = params || {};
            if (CURRENT_TOUCH[0]) {
                params.iniTouch = (GESTURE.fingers > 1 ? FIRST_TOUCH : FIRST_TOUCH[0]);
                params.currentTouch = (GESTURE.fingers > 1 ? CURRENT_TOUCH : CURRENT_TOUCH[0]);
            }
            return GESTURE.el.trigger(type, params);
        }
    };

    _cleanGesture = function () {
        FIRST_TOUCH = [];
        CURRENT_TOUCH = [];
        GESTURE = {};
        return clearTimeout(TOUCH_TIMEOUT);
    };

    _angle = function (touches_data) {
        var A = touches_data[0],
            B = touches_data[1],
            angle = Math.atan((B.y - A.y) * -1 / (B.x - A.x)) * (180 / Math.PI);
        return (angle < 0) ? angle + 180 : angle;
    };

    _distance = function (touches_data) {
        var A = touches_data[0],
            B = touches_data[1];
        return Math.sqrt((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y)) * -1;
    };

    _getTouches = function (event) {
        var e = event.originalEvent || event;
        return e.touches || [e];
    };

    _parentIfText = function (node) {
        return ("tagName" in node) ? node : node.parentNode;
    };

    _swipeDirection = function (x1, x2, y1, y2) {
        if (Math.abs(x1 - x2) >= Math.abs(y1 - y2)) {
            return (x1 - x2 > 0) ? "Left" : "Right";
        } else {
            return (y1 - y2 > 0) ? "Up" : "Down";
        }
    };

    _hold = function () {
        if (GESTURE.last && (Date.now() - GESTURE.last >= HOLD_DELAY)) {
            _trigger("hold");
            return _cleanGesture();
        }
    };
}(jQuery));


if ($("*").is("#constructor")) {

    var table_list = [];
    var date = "";
    var time = "";
    var $container = $("#constructor");
    var hall = $container.data("hall");

    var canvas = new fabric.Canvas('constructor', {
        allowTouchScrolling: true,
    });

    window.addEventListener('resize', resize, false);
    fabric.Object.prototype.transparentCorners = false;
    canvas.selection = false;
    fabric.Object.prototype.cornerColor = 'transparent';
    fabric.Object.prototype.cornerStyle = 'circle';
    fabric.Object.prototype.hasControls = false;
    fabric.Object.prototype.hasBorders = false;


    function resize() {
        canvas.setHeight($(".canvas-container").height());
        canvas.setWidth($(".canvas-container").width());
        canvas.renderAll();
    }

    resize();

    var movaNo = false;

    window.zoogetra = null;
    var timouts = null;
    $(".canvas-container").on('pinching', function (event) {
        zoom_controls = true;

        if (window.zoogetra == null) {
            window.zoogetra = canvas.getZoom();
        }
        if (typeof window.distance != "undefined") {
            var zoom = canvas.viewportTransform[0];
            var x_s = canvas.viewportTransform[4];
            var y_s = canvas.viewportTransform[5];
            var zoomX = (1 / zoom);
            var zoomY = (1 / zoom);


            var zoom = window.zoogetra;

            var x = ((((x_s * zoomX) * -1) + (($("#constructor").width() / 2) * zoomX)) / 2);

            var y = (((y_s * zoomY) * -1) + (($("#constructor").height() / 2) * zoomY) / 2);

            zoom *= 0.999 ** ((window.distance) * -1);
            canvas.zoomToPoint({x: 0, y: 0}, zoom);
        }
    }).on('pinchIn', function (event) {
        if (timouts != null) {
            clearTimeout(timouts);
        }
        timouts = setTimeout(function () {
            zoom_controls = false;
            timouts = null;
        }, 300);
        window.zoogetra = null;
    }).on('pinchOut', function (event) {
        window.zoogetra = null;
        if (timouts != null) {
            clearTimeout(timouts);
        }
        timouts = setTimeout(function () {
            zoom_controls = false;
            timouts = null;
        }, 300);
    });

    var table_list_on = "";

    canvas.on({

        'mouse:down': function (e) {
            movaNo = false;
        },
        'mouse:up': function (e) {
            if (e.target) {
                if (!movaNo && !zoom_controls) {
                    if (typeof e.target["values"]["id"] != "undefined") {
                        if (e.target.opacity == 0.3) {
                            e.target.opacity = 1;
                            var index = table_list.indexOf(e.target["values"]["id"]);
                            if (index > -1) {
                                table_list.splice(index, 1);
                                // console.log(table_list);
                            }
                            table_list_on = "";
                        } else {
                            if (e.target["values"]["type"]) {
                                e.target.opacity = 0.3;
                                table_list.push(e.target["values"]["id"]);
                                table_list_on = "";
                            } else {
                                table_list_on = e.target["values"]["id"];
                            }
                        }
                        // + ";time=" + time + ";date=" + date + ";" ;
                        window.location.href = location.origin + location.pathname + window.location.search + "#tables=" + table_list.join(",") + ";" + (table_list_on != "" ? "targs=" + table_list_on + ";" : "");
                        canvas.renderAll();
                    }
                }
            }


        }
    });

    var offset_xs = 0;

    var pausePanning = true;
    var zoomStartScale = 0;


    canvas.on('mouse:move', function (opt) {
        // var delta = opt.e.deltaY;
        // var zoom = canvas.getZoom();
        // zoom *= 0.999 ** delta;
        // if (zoom > 20) zoom = 20;
        // if (zoom < 0.01) zoom = 0.01;
        //
        // $("#consa2").html(opt.e.offsetX);
        // // offset_xs += opt.e.offsetX;
        // // canvas.zoomToPoint({x: opt.e.offsetX, y: opt.e.offsetY}, zoom);
        // opt.e.preventDefault();
        // opt.e.stopPropagation();
    });

    var maxx_zoom = ($container.width() / parseInt(hall["schema_size"]));
    var current_width = parseInt(hall["schema_size"]);
    var zooCurrent = maxx_zoom;
    canvas.on('mouse:wheel', function (opt) {
        var delta = opt.e.deltaY;

        var zoom = canvas.getZoom();
        zoom *= 0.999 ** delta;
        if (zoom > 20) zoom = 20;
        if (zoom < 0.01) zoom = 0.01;

        if (zoom <= maxx_zoom) {
            zoom = maxx_zoom;
        }

        current_width = parseInt(hall["schema_size"]) * (maxx_zoom / zoom);
        zooCurrent = zoom;

        offset_xs += opt.e.offsetX;
        canvas.zoomToPoint({x: opt.e.offsetX, y: opt.e.offsetY}, zoom);
        opt.e.preventDefault();
        opt.e.stopPropagation();
    });


    function zoomm_start() {
        var zoom = canvas.getZoom();
        var zoomX = ($container.width() / parseInt(hall["schema_size"]));
        // $container.css("background-size", (((app.stage.scale.x == 0 ? 1 : app.stage.scale.x) * ((window.bk_size)) + "px")));
        canvas.setZoom(zoomX);
    }

    zoomm_start();

    const img_wh = new Image();
    var height_propers = 0;
    img_wh.onload = function () {
        height_propers = (this.height / this.width);
        fabric.Image.fromURL(hall["schema"], function (myImg) {
            //i create an extra var for to change some image properties
            var img5 = myImg.set({
                left: 0,
                top: 0,
                values: false,
                selectable: false,
                scaleX: (parseInt(hall["schema_size"]) / myImg.width),
                scaleY: (parseInt(hall["schema_size"]) * height_propers) / myImg.height,
            });
            canvas.add(img5);
        });
    }

    img_wh.src = hall["schema"];

    function zoom(s, x, y) {

    }

    function jsend(valuehg, dataelx, evalx, errorj, type) {

        var CSRF_TOKEN = "",
            dateof = {_token: CSRF_TOKEN},
            deteextend = $.extend(dataelx, dateof);


        if (typeof evalx == "undefined") {
            evalx = "";
        }

        if (typeof errorj == "undefined") {
            errorj = "";
        }
        if (typeof type == "undefined") {
            type = "POST";
        }

        $.ajax({
            /* the route pointing to the post function */
            url: valuehg,
            type: type,
            /* send the csrf-token and the input to the controller */
            data: deteextend,
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {

                eval(evalx);

            },
            error: function (data) {
                eval(errorj);
            }
        });
    }

    function createGraphics(settingIn) {

    }


    function onMoveStart(event) {
        // event.stopPropagation();
        //
        // if (this.alpha == 0.3) {
        //     this.alpha = 1;
        //     var index = table_list.indexOf(this.data.id);
        //     if (index > -1) {
        //         table_list.splice(index, 1);
        //         // console.log(table_list);
        //     }
        // } else {
        //     this.alpha = 0.3;
        //     table_list.push(this.data.id);
        //
        // }
        //
        // window.location.href = location.origin + location.pathname + "#tables=" + table_list.join(",") + ";time=" + time + ";date=" + date + ";";
        //
        // this.dragging = true;
    }

    date = $('#date').val();
    $(document).on('change', '#date', function (e) {
        date = $(this).val();
        // + ";time=" + time + ";date=" + date + ";";
        window.location.href = location.origin + location.pathname + window.location.search + "#tables=" + table_list.join(",") + ";"
    });

    time = $('#time').val();
    $(document).on('change', '#time', function (e) {
        time = $(this).val();
        // time=" + time + ";date=" + date + ";"
        window.location.href = location.origin + location.pathname + window.location.search + "#tables=" + table_list.join(",") + ";";
    });


    var asdasd = 0;

    function startPan(event) {

        var x0 = event.targetTouches[0].pageX,
            y0 = event.targetTouches[0].pageY;

        var x_position_start = canvas.viewportTransform[4];
        var y_position_start = canvas.viewportTransform[5];

        function continuePan(event) {
            var x = event.targetTouches[0].pageX,
                y = event.targetTouches[0].pageY;

            var x_position = canvas.viewportTransform[4];
            var y_position = canvas.viewportTransform[5];
            var controlr = (x_position_start - x_position) + (y_position_start - y_position);
            if (!zoom_controls) {
                // console.log(canvas.getObjects());
                var plisy = y - y0;
                var plisx = x - x0;
                var y_pre = plisy;
                var x_pre = plisx;

                //
                var width_current = (parseInt(hall["schema_size"]) * canvas.viewportTransform[0]);
                var height_current = ((parseInt(hall["schema_size"]) * height_propers) * canvas.viewportTransform[0]);
                // console.log(width_current - (width_current + x_position));
                // console.log(width_current + x_position);
                // var width_s = ($container.width() * (maxx_zoom / canvas.getZoom()));


                // console.log((width_current + x_position) - (width_s ));
                // console.log(width_s - x_position, " ", width_current);

                // if(x_position){
                //
                // }


                if ((height_current + (y_position + plisy)) < 0) {
                    plisy = 0;
                } else if (y_position + plisy >= 0) {
                    plisy = y_position * (-1);
                }

                if ((width_current + (x_position + plisx)) < 0) {
                    plisx = 0;
                } else if (x_position + plisx >= 0) {
                    plisx = x_position * (-1);
                }
                canvas.relativePan({x: plisx, y: plisy});
                x0 = x;
                y0 = y;
                if (controlr > 10 || controlr < -10) {
                    movaNo = true;
                } else {
                    movaNo = false;
                }
            }
        }

        function stopPan(event) {
            $(window).off('touchmove', continuePan);
            $(window).off('touchend', stopPan);
        };
        $(window).on("touchmove", continuePan);
        $(window).on("touchend", stopPan);
        $(window).contextmenu(cancelMenu);
    };

    function cancelMenu() {
        $(window).off('contextmenu', cancelMenu);
        return false;
    }

    $(".canvas-container").on('touchstart', startPan);

    window.list_array = [];

    function add_object(data) {

        $.each(data.data, function (index, element) {

            if (typeof window.list_array[element["id"]] == "undefined") {

                var control_hell = true;
                $.each(window.reserv, function (index_is, element_is) {
                    if (element["id"] == element_is["restaurant_scheme_id"]) {
                        control_hell = false;
                    }
                });
                fabric.Image.fromURL(control_hell ? element["icon"] : element["iconbooked"], function (myImg) {
                    var img1 = myImg.set({
                        left: parseFloat(element["offsetx"]),
                        top: parseFloat(element["offsety"]),
                        values: {
                            id: element["id"],
                            type: control_hell,
                            scaleX: element["scalex"],
                            scaleY: element["scaley"],
                            angle: parseFloat(element["angle"]),
                            scheme_elements_id: element["scheme_elements_id"],
                        },
                        scaleX: element["scalex"],
                        scaleY: element["scaley"],
                        angle: (typeof element["angle"] != "undefined" ? element["angle"] : 0),
                    });
                    window.list_array[element["id"]] = true;
                    img1.lockMovementX = true;
                    img1.lockMovementY = true;
                    canvas.add(img1);
                });
            }
        });

    }

    function ajax_object() {
        jsend('/api/admin/get_point/' + hall["id"], {}, "add_object(data);", "", "GET")
    }

    window.reserv_get = function (data) {

        window.reserv = data.data;
        ajax_object();
    }

    window.chenchdate = function () {
        var sen_inputs = {
            "id": hall["restaurant_id"],
            "from": $("#date").val() + "T" + $("#time").val(),
            "to": $("#date").val() + "T" + $("#time").val(),
            "hall": hall["id"],
        };
        jsend('/api/reserved', sen_inputs, "reserv_get(data);", "", "POST")
    }

    $(document).on('change', '.date_box', function (e) {
        window.chenchdate();
    });
    window.chenchdate();
} else {
    $(document).on('change', '.city_ck', function (e) {
        window.location.href = location.origin + location.pathname + "#city=" + $("#city").val() + ";date=" + $("#date").val();
    });
    $(document).on('change', '.date_ck', function (e) {
        window.location.href = location.origin + location.pathname + "#city=" + $("#city").val() + ";date=" + $("#date").val();
    });
    if ($("*").is("#maps")) {

        let location_s = [];
        if (typeof window.location["search"] != "undefined") {
            let locationSplit = window.location["search"];
            locationSplit = locationSplit.split("&");
            locationSplit = locationSplit[0].split("location=");

            if (locationSplit.length > 1) {
                locationSplit = locationSplit[1];
                locationSplit = (decodeURI(locationSplit));
                if (/^[\],:{}\s]*$/.test(locationSplit.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                    location_s = JSON.parse(locationSplit);
                } else {

                    //the json is not ok
                }
            }
        }

        ymaps.ready(function () {
            window.maspInit = new mapinit();
            window.maspInit.init("maps");
            if (typeof location_s["latitude"] != "undefined") {
                 window.maspInit.setCenter("maps", [location_s["latitude"], location_s["longitude"]], 18);
            }

            setTimeout(function () {
                if (typeof location_s["latitude"] != "undefined") {

                    window.maspInit.addPointCustom("maps", [location_s["latitude"], location_s["longitude"]], '/YaRu.svg')
                }

                $.each($("#maps").data("info"), function (index, val) {
                    // 43.258355, 76.920410

                    window.maspInit.addPoint("maps", {
                        balloonContent: '<div class="data_click" style="display:none;">' + (val["id"]) + '</div><div class="centers" style="display: flex;width: 100%;text-align: center;">' + val["name"] + '</div>',
                        coordinates: [val["longitude"], val["latitude"]]
                    });
                });
            }, 1000);
        });
    }
}

