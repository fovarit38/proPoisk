export function get_user() {

    var return_ = null;
    var _token = localStorage.getItem('token');
    $.ajax({
        url: '/api/auth/user',
        type: "GET",
        async: false,
        cache: false,
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + _token
        },
        success: function (res) {
            return_ = res;
        },
        error: function (res) {
            return_ = res;
        }
    });
    return return_;
}


export function inArray(arr, row, search) {
    return arr.filter(function (item, i, arr) {
        return (item[row] == search);
    });
}

export function serialize(form) {
    var unindexed_array = new FormData(form);

    return unindexed_array;
}


export function def_event() {

    $(document).on("click", ".cli", function (e) {
        var
            $this = $(this),
            closest = $this.data("closest"),
            closestCli = $this.data("closestcli"),
            unDefain = $this.data("un"),
            target = $this.data("target"),
            $target = $this.data("target");

        if (typeof closest != "undefined") {
            $this = $($this.closest(closest));
        }


        if (typeof unDefain == "undefined") {
            unDefain = 0;
        } else {
            unDefain = 1;
        }

        if (typeof target != "undefined") {
            $target = $(target);
        }


        if (typeof closestCli != "undefined") {


            $(".cli", $this.closest(closestCli)).removeClass("active");
            $this.addClass("active");


        } else if (unDefain == 0) {

            if ($this.hasClass("active")) {
                $this.removeClass("active");
            } else {
                $this.addClass("active");
            }

        } else if (unDefain != 0) {


            $this.addClass("active").siblings().removeClass("active");


            if (typeof target != "undefined") {


                $target.eq($this.index()).addClass("active").siblings().removeClass("active");

            }

        }
    });

}
