require('./bootstrap');
import Vue from "vue";
import router from "./router";
import Notifications from 'vue-notification';
import YmapPlugin from 'vue-yandex-maps'
Vue.use(Notifications);
const options  = {
    apiKey: 'a2a8abd6-66b5-4670-8e3a-4df11e81e634',
    lang: 'ru_RU',
    coordorder: 'latlong',
    version: '2.1'
}
Vue.use(YmapPlugin, options)



import {get_user} from "../../overall/js/app";


import VModal from 'vue-js-modal';
Vue.use(VModal);


var user_auth = get_user();
window.user_get=user_auth;
if (user_auth.success) {
    //Мастер страница Админка

    Vue.component('layouts', require('./views/layouts/app').default);
    const app = new Vue({
        el: '#app',
        router: router
    });
} else {
    //Мастер страница Авторизация Админка
    Vue.component('layouts', require('./views/auth/layouts/app').default);
    const app = new Vue({
        el: '#app',
        router: router
    });
}





