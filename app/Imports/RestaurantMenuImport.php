<?php

namespace App\Imports;

use App\Models\RestaurantMenu;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class RestaurantMenuImport implements ToCollection
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function collection(Collection $rows)
    {
        $rows = json_encode($rows);
        $rows = json_decode($rows, true);

        unset($rows[0]);
        \App\Models\RestaurantMenu::where("restaurant_id", $this->id)->delete();
        foreach ($rows as $row) {

            $resMenu = new \App\Models\RestaurantMenu;
            $resMenu->category = $row[0];
            $resMenu->name = $row[1];
            $resMenu->price = $row[2];
            $resMenu->content = $row[3];
            $resMenu->restaurant_id = $this->id;
            $resMenu->save();
        }
    }
}
