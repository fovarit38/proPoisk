<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;
use Schema;
use NCANodeClient;
use CRest;
use Illuminate\Support\Facades\Log;
use Image;
use function PHPUnit\Framework\isEmpty;

class MainController extends Controller
{


    public function __construct()
    {

    }


    public function main()
    {

        return view('client.main');
    }


    public function admin_main()
    {

        return view('admin.main');
    }




}
