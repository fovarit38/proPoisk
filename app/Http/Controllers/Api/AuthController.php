<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Log;
use Mail;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{


    protected $users;


    function validate_phone_number($phone)
    {

        $phone = str_replace("-", "", filter_var($phone, FILTER_SANITIZE_NUMBER_INT));
        if (mb_substr($phone, 0, 1) == "9" || mb_substr($phone, 1, 1) == "9") {

        } else {

            if (mb_substr($phone, 0, 1) == "8") {
                $phone = mb_substr($phone, 1, mb_strlen($phone));
            } else if (mb_substr($phone, 0, 1) == "+") {
                $phone = mb_substr($phone, 2, mb_strlen($phone));
            } else if (mb_substr($phone, 0, 1) == "7" && strlen($phone) == 11) {
                $phone = mb_substr($phone, 1, mb_strlen($phone));
            }

        }
        $phone = preg_replace("/[^0-9]/", '', $phone);

        if (strlen($phone) != 10 && strlen($phone) != 12 && strlen($phone) != 11) {
            return false;
        } else {
            return $phone;
        }
    }

    public function get_user()
    {
        $this->users = auth()->guard('api')->user();
        if (empty($this->users)) {

            return $this->sendError('вы не авторизованы');
        } else {
            return $this->sendResponse('вы авторизованы', $this->users);
        }
    }


    public function auth_login_mobile(Request $request)
    {

        $auth_user = \App\Models\User::where("email", $request->email)->where("status", "1")->first();
        if (!is_null($auth_user)) {
            $passed = Hash::check($request->password, $auth_user->password);
            if ($passed) {
                Auth::login($auth_user);
                $user = Auth::user();
                $success = $user->toarray();
                $success['token'] = $user->createToken(env("APP_NAME"))->accessToken;


                return $this->sendResponse($success, 'Вы авторизированны успешно.');
            } else {
                return $this->sendError('Error', ['error' => 'Введен неверный Email или пароль']);
            }
        } else {
            return $this->sendError('Error', ['error' => 'Введен неверный телефон или код']);
        }
    }

    public function auth_login(Request $request)
    {

        $nameInput = "login";
        $fieldName = "";
        $identity = $request->login;
        if (filter_var($identity, FILTER_VALIDATE_EMAIL)) {
            $fieldName = "email";
        } else {
            $fieldName = validate_phone_number($identity) == false ? 'username' : 'tel';
        }
        if ($fieldName == "tel") {
            $identity = validate_phone_number($identity);
        }

        $success = [];


        if (Auth::attempt([$fieldName => $identity, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] = $user->createToken(env("APP_NAME"))->accessToken;

            if (isset($request->token) && \App\Models\TokenList::where("token", $request->token)->count() == 0) {
                $tokens = new \App\Models\TokenList;
                $tokens->token = $request->token;
                $tokens->user_id = $user->id;
                $user->save();
            }
            return $this->sendResponse($success, 'Вы авторизированны успешно.');
        } else {
            return $this->sendError('Error', ['error' => 'Введен неверный логин или пароль']);
        }
    }


    public function register(Request $request)
    {

        $fieldName = "email";

        if (!isset($request->code)) {
            $identity = ($request->email);

            $code = random_int(10000, 99999);

            request()->merge([$fieldName => $identity]);

            $input = $request->all();

//            ->where("status", "0")
//            $phone = \App\Models\User::where("email", $identity)->delete();

            if (isset($input["email"])) {
                $phone = \App\Models\User::where("email", $identity)->first();
                if (!is_null($phone)) {
                    return $this->sendError('Error', ['error' => 'Email уже занят']);
                }
            }


            Mail::raw('Код для регистрации ' . $code, function ($message) use ($identity) {
                $message->to($identity)
                    ->subject('Код для регистрации в PRO поиск');
            });

            request()->merge(["code" => $code]);
            if ($request->password != "rander") {
                $input['password'] = bcrypt($request->password);
            } else {
                $input['password'] = ($request->password);
            }

            $input['status'] = "0";
            $input['code'] = $code;
            if (isset($request->tel)) {
                if(validate_phone_number($request->tel)){
                    $input['tel'] = validate_phone_number($request->tel);
                }
            }
            if (isset($request->nameUser)) {
                $input['name'] = $request->nameUser;
            }
            $user = \App\Models\User::create($input);


            return $this->sendResponse([], 'Код отправлен.');

        } else {

            $phone = \App\Models\User::where("email", $request->email)->where("status", "0")->where("code", $request->code)->first();
            if (!is_null($phone)) {
                $phone->status = 1;
                $phone->save();

                $success = $phone->toarray();
                $token = $phone->createToken('burgerApp')->accessToken;
                $success["token"] = $token;

                if ($phone->password == "rander") {
                    $code = random_int(100000, 999999);
                    $phone->password = Hash::make($code);
                    $phone->save();
                    Mail::raw('Пароль от приложения Pro Поиск: ' . $code, function ($message) use ($phone) {
                        $message->to($phone->email)
                            ->subject('Пароль от приложения Pro Поиск');
                    });
                }


                return $this->sendResponse($success, 'Код Подтвержден ');
            }
            return $this->sendError('Error', ['error' => 'Код не найден']);

        }

    }


}
