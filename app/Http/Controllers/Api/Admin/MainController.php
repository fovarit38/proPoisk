<?php


namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use NCANodeClient;
use Carbon\Carbon;
use function PHPUnit\Framework\isEmpty;
use Storage;
use Illuminate\Support\Facades\Hash;
use Mail;

class MainController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Товары в корзине
    protected $users;


    public function __construct(Request $request)
    {
//        $this->users = auth()->guard('api')->user();
//
//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }

    public function update_files(Request $request)
    {

        $files = $request;
        $request = $request->all();
        if (isset($_FILES["files"])) {
            foreach ($_FILES["files"]["name"] as $nameInput => $fileout) {

                if ($_FILES["files"]["error"][$nameInput] == 0) {
                    $filename = Str::random(6) . "." . $files["files"][$nameInput]->getClientOriginalExtension();
                    $files_is = Storage::disk('uploads')->put($filename, file_get_contents($files["files"][$nameInput]->getRealPath()));
                    if ($files_is) {
                        $request["files"][$nameInput] = "/storage/uploads/" . $filename;
                    } else {
                        unset($request["save"][$nameInput]);
                    }
                } else {
                    unset($request["save"][$nameInput]);
                }
            }
        }
        foreach ($request["files"] as $fileas) {
            if (is_string($fileas)) {
                $medias = new \App\Models\Media;
                $medias->restaurant_id = $request["id"];
                $medias->images = $fileas;
                $medias->save();
            }
        }
    }


    public function inputs()
    {

    }

    public function order_status(Request $request)
    {
        $request = $request->all();

        $save_order = \App\Models\ReservationOrder::find($request["id"]);

        if (!is_null($save_order)) {
            if ($request["message_on"] == "1") {

                $us_user = \App\Models\User::where("tel", $save_order->tel)->first();

                foreach (\App\Models\User::where("restaurant_id", $save_order->getRawOriginal('restaurant_id'))->get() as $user_res) {
                    push([
                        "to" => "/topics/User_" . $user_res->id,
                        "title" => "Информация о обновленна",
                        "subtitle" => $request["message"],
                        "body" => $request["message"]
                    ], $user_res->id);
                }

                if (is_null($us_user)) {
                    $data = array(
                        'login' => "TeamGis",
                        'psw' => "gIs2021",
                        'phones' => '7' . validate_phone_number($save_order->tel),
                        'mes' => $request["message"],
                    );

                    $info = file_get_contents('https://smsc.kz/sys/send.php?' . http_build_query($data));

                } else {


                    push([
                        "to" => "/topics/User_" . $us_user->id,
                        "title" => $request["title"],
                        "subtitle" => $request["message"],
                        "body" => $request["message"]
                    ], (!is_null($us_user) ? $us_user->id : null));

                }
            }
            $save_order->status = $request["status"];
            $save_order->save();

            if ($request["status"] == "Reject" || $request["status"] == "Сompleted" || $request["status"] == "Cancel") {
                \App\Models\Reservation::where("order_key", $save_order->id)->delete();
            }


        }


        return $this->sendResponse($request, '');
    }

    public function profile_reset(Request $request)
    {
        $request = $request->all();
        $code = random_int(100000, 999999);
        $users = \App\Models\User::where("email", $request["email"])->first();
        $users->password = Hash::make($code);
        $users->save();

        Mail::raw('Пароль от приложения Pro Поиск изменен на: ' . $code, function ($message) use ($users) {
            $message->to($users->email)
                ->subject('Пароль от приложения Pro Поиск изменен на');
        });

        return $this->sendResponse($users, 'Пароль измнен');
    }

    public function profile_save(Request $request)
    {
        $request = $request->all();
        $users = auth()->guard('api')->user();
        if (is_null($users) || !isset($users->id) || ($users) == "") {
            return $this->sendError('вы не авторизованы');
        }

        if (isset($request["images"])) {
            $users->image = $request["images"];
            $users->save();
        }

        if (isset($request["name"])) {
            $users->name = $request["name"];
            $users->save();
        }
        if (isset($request["foname"])) {
            $users->foname = $request["foname"];
            $users->save();
        }
        if (isset($request["password"])) {
            if (Hash::check($request["oldPass"], $users->password)) {
                $users->password = Hash::make($request["password"]);
                $users->save();
            } else {
                return $this->sendError('Пароль не совпадает');
            }
        }

        if (isset($request["tel"])) {
            if (validate_phone_number($request["tel"])) {
                $users->tel = validate_phone_number($request["tel"]);
                $users->save();
            }
        }

        return $this->sendResponse($users, 'вы авторизованы');

    }

    public function feedback_save(Request $request)
    {
        $users = auth()->guard('api')->user();
        if (is_null($users) || !isset($users->id) || ($users) == "") {
            return $this->sendError('вы не авторизованы');
        }

        $request = $request->all();
        $feedback = new \App\Models\Feedback;
        $feedback->user_id = $users->id;
        $feedback->user_name = $request["name"];
        $feedback->content = $request["content"];
        $feedback->tel = $request["tel"];
        $feedback->save();

        return $this->sendResponse([], '');
    }

    public function card_add()
    {

        $users = auth()->guard('api')->user();
        $this->users = auth()->guard('api')->user();
        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
            return $this->sendError('вы не авторизованы');
        }

//        $order = order_generated(
//            [
//                "orderNumber" => $order_pay->id,
//                "amount" => (int)$order_pay->order_price,
//            ],
//            "https://securepayments.sberbank.kz/payment/rest/paymentOrderBinding.do?"
//        );

    }

    public function order_list()
    {
        $users = auth()->guard('api')->user();
        $this->users = auth()->guard('api')->user();
        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
            return $this->sendError('вы не авторизованы');
        }
        $restorans = \App\Models\ReservationOrder::where("user_id", $this->users->id)->orderby("id", "desc")->get();
        return $this->sendResponse($restorans, '');
    }


    public function order_success(Request $request)
    {
        $request = $request->all();
        $reservations = \App\Models\ReservationOrder::where("orderPayID", $request["orderId"])->first();
        if (empty($reservations)) {
            return $this->sendError('Заказ не найден');
        }

        $reservations->status_pay = "done";
        $reservations->save();

        foreach (\App\Models\User::where("restaurant_id", $reservations->getOriginal('restaurant_id'))->get() as $user_res) {
            $tokens_id = \App\Models\TokenList::where("user_id", $user_res->id)->get();
            if (count($tokens_id) > 0) {
                foreach ($tokens_id as $tokensx) {
                    push([
                        "to" => $tokensx->token,
                        "title" => "Подана заявка на бронь ",
                        "subtitle" => "Заявка на столы:" . $reservations->desk . " ",
                        "body" => "Заявка на столы:" . $reservations->desk . " ",
                    ], $user_res->id);
                }
            } else {
                push([
                    "to" => "/topics/User_" . $user_res->id,
                    "title" => "Подана заявка на бронь ",
                    "subtitle" => "Заявка на столы:" . $reservations->desk . " ",
                    "body" => "Заявка на столы:" . $reservations->desk . " ",
                ], $user_res->id);
            }
        }


        $us_user = \App\Models\User::where("tel", validate_phone_number($reservations->tel))->first();
        $restaurant = \App\Models\Restaurant::find($reservations->getRawOriginal('restaurant_id'));
        if (!is_null($us_user)) {
            if (!is_null($restaurant)) {
                $tokens_id = \App\Models\TokenList::where("user_id", $us_user->id)->get();

                if (count($tokens_id) > 0) {
                    foreach ($tokens_id as $tokensx) {
                        push([
                            "to" => $tokensx->token,
                            "title" => "Вы подали заявку на бронь в " . $restaurant->name,
                            "subtitle" => "Заявка на " . $reservations->desk . " ",
                            "body" => "Заявка на " . $reservations->desk . " ",
                        ], $us_user->id);
                    }
                } else {
                    push([
                        "to" => "/topics/User_" . $us_user->id,
                        "title" => "Вы подали заявку на бронь в " . $restaurant->name,
                        "subtitle" => "Заявка на " . $reservations->desk . " ",
                        "body" => "Заявка на " . $reservations->desk . " ",
                    ], $us_user->id);
                }


            }
        } else {
            if (validate_phone_number($request["tel"]) !== false) {
                $data = array(
                    'login' => "TeamGis",
                    'psw' => "gIs2021",
                    'phones' => '7' . validate_phone_number($request["tel"]),
                    'mes' => "Имя " . $reservations->name . " благодарим Вас за выбор нашего заведения. мы ждем Вас в ресторане/кафе " . $reservations->restaurant_id . " " . $reservations->date . ". Будем рады видеть Вас и для Вашего удобства в будущем рекомендуем скачать приложение http://onelink.to/d5py96",
                );
                $info = file_get_contents('https://smsc.kz/sys/send.php?' . http_build_query($data));
            }
        }

        $message = "Поступила заявка Бронь <b>Оплачена</b>\n";
        $message .= 'Телефон: +7' . $reservations->tel . "\n";
        $message .= 'Имя: ' . $reservations->name . "\n";
        $message .= 'Завеление: ' . $reservations->restaurant_id . "\n";
        $message .= 'Столы: ' . $reservations->desk . "\n";
        $message .= 'Персон: ' . $reservations->person . "\n";
        if (isset($reservations->date)) {
            $message .= "Дата и время брони:\n" . $reservations->date . "\n";
        }

        bot('1058388746:AAELeUAtJF8Uk2qX2gc4iHwxbp4K1VYiqYE', '-1001237901047', $message);

    }


    public function order_error(Request $request)
    {
        $request = $request->all();
        $reservations = \App\Models\ReservationOrder::where("orderPayID", $request["orderId"])->first();
        if (empty($reservations)) {
            return $this->sendError('Заказ не найден');
        }
        $reservations->status_pay = "error";
        $reservations->save();

        \App\Models\Reservation::where("order_key", $reservations->id)->delete();
    }

    public function order_pay(Request $request)
    {
        $request = $request->all();
        $users = auth()->guard('api')->user();
        $this->users = auth()->guard('api')->user();
        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
            $this->middleware(function ($request, $next) {
                return $this->sendError('вы не авторизованы');
            });
        }

        $order_pay = \App\Models\ReservationOrder::find($request["id"]);
        if (empty($order_pay)) {
            return $this->sendError('заявка не найдена');
        }


        if ($order_pay->status_pay == "payment") {
            return $this->sendResponse(["formUrl" => "https://3dsec.sberbank.kz/payment/merchants/Teamgis/payment_ru.html?mdOrder=" . $order_pay->orderpayid, "status" => "pay"], '');
        } else {

        }

        if (!empty(str_replace(" ", "", $order_pay->order_price)) && (int)$order_pay->order_price > 0) {
            $order = order_generated(
                [
                    "orderNumber" => $order_pay->id,
                    "amount" => (int)$order_pay->order_price,
                ]
            );
            if (!isset($order["errorCode"])) {
                $order_pay->orderpayid = $order["orderId"];
                $order_pay->status_pay = "payment";
                $order_pay->save();
                return $this->sendResponse(["formUrl" => $order["formUrl"], "status" => "pay"], '');
            } else {
                return $this->sendError(["data" => $order, "status" => "error"]);
            }
        } else {


            $order_pay->status_pay = "done";
            $order_pay->save();
            return $this->sendResponse(["status" => "done"], '');
        }

    }

    public function order_save(Request $request)
    {
        $request = $request->all();


        $users = auth()->guard('api')->user();


        $restaurantHall = \App\Models\RestaurantScheme::wherein("id", explode(",", $request["hallHover"]))->get();
        $restaurant = \App\Models\Restaurant::find($request["restaurant_id"]);


        $restorans = new \App\Models\ReservationOrder;

        $restorans->desk = $request["hallHover"];
        $restorans->name = isset($request["name"]) ? $request["name"] : $users->name;
        $restorans->tel = isset($request["tel"]) ? validate_phone_number($request["tel"]) : $users->tel;
        $restorans->person = $request["person"];

        $us_user = \App\Models\User::where("tel", $restorans->tel)->first();

        if (isset($restaurant->check)) {
            if ((int)$restaurant->check > 0) {
                $restorans->status_pay = "processing";
                $restorans->order_price = (int)$restaurant->check * count(explode(",", $request["hallHover"]));
            } else {
                $restorans->status_pay = "done";
                $restorans->order_price = "0";
            }
        } else {
            $restorans->status_pay = "done";
            $restorans->order_price = "0";
        }

        if ($restorans->status_pay == "done") {
            if (!is_null($restaurant)) {
                $message = "Поступила заявка Бронь бесплатная\n";
                $message .= 'Телефон: +7' . $restorans->tel . "\n";
                $message .= 'Имя: ' . $restorans->name . "\n";
                $message .= 'Завеление: ' . $restaurant->name . "\n";
                $message .= 'Столы: ' . $restorans->desk . "\n";
                $message .= 'Персон: ' . $restorans->person . "\n";
                if (isset($request["data"]) && isset($request["time"])) {
                    $message .= "Дата и время брони:\n" . Carbon::parse($request["data"] . ' ' . $request["time"])->format('Y-m-d H:i:s') . "\n";
                }

                bot('1058388746:AAELeUAtJF8Uk2qX2gc4iHwxbp4K1VYiqYE', '-1001237901047', $message);
            }
            foreach (\App\Models\User::where("restaurant_id", $restaurant->id)->get() as $user_res) {

                $tokens_id = \App\Models\TokenList::where("user_id", $user_res->id)->get();
                if (count($tokens_id) > 0) {
                    foreach ($tokens_id as $tokensx) {
                        push([
                            "to" => $tokensx->token,
                            "title" => "Подана заявка на бронь ",
                            "subtitle" => "Заявка на столы:" . $restorans->desk . " ",
                            "body" => "Заявка на столы:" . $restorans->desk . " ",
                        ], $user_res->id);
                    }
                } else {
                    push([
                        "to" => "/topics/User_" . $user_res->id,
                        "title" => "Подана заявка на бронь ",
                        "subtitle" => "Заявка на столы:" . $restorans->desk . " ",
                        "body" => "Заявка на столы:" . $restorans->desk . " ",
                    ], $user_res->id);
                }
            }


            if (!is_null($us_user)) {
                if (!is_null($restaurant)) {
                    push([
                        "to" => "/topics/User_" . $us_user->id,
                        "title" => "Вы подали заявку на бронь в " . $restaurant->name,
                        "subtitle" => "Заявка на " . $restorans->desk . " ",
                        "body" => "Заявка на " . $restorans->desk . " ",
                    ], $us_user->id);
                }
            } else {
                if (isset($request["status"])) {
                    $restorans->status = $request["status"];
                    if (validate_phone_number($request["tel"]) !== false) {
                        $data = array(
                            'login' => "TeamGis",
                            'psw' => "gIs2021",
                            'phones' => '7' . validate_phone_number($request["tel"]),
                            'mes' => "Имя " . $restorans->name . " благодарим Вас за выбор нашего заведения. мы ждем Вас в ресторане/кафе " . $restorans->restaurant_id . " " . $restorans->date . ". Будем рады видеть Вас и для Вашего удобства в будущем рекомендуем скачать приложение http://onelink.to/d5py96",
                        );
                        $info = file_get_contents('https://smsc.kz/sys/send.php?' . http_build_query($data));
                    }
                }


            }

        }


        $hall_id = 0;

        $restorans->user_id = $users->id;
        $restorans->restaurant_id = $request["restaurant_id"];
        if (isset($request["_token"])) {
            $restorans->_token = $request["_token"];
        }
        $restorans->date = date('Y-m-d H:i:s', strtotime($request["data"] . ' ' . $request["time"]));
        $restorans->save();


        $stol = explode(",", $request["hallHover"]);
        foreach ($stol as $st) {
            $randers_box = new \App\Models\Reservation;
            $randers_box->date_from = Carbon::parse($request["data"] . ' ' . $request["time"])->format('Y-m-d H:i:s');
            $randers_box->date_to = Carbon::parse($request["data"] . ' ' . $request["time"])->format('Y-m-d H:i:s');
            $randers_box->name = $restorans->name;
            $randers_box->tel = $restorans->tel;
            $randers_box->restaurant_id = $request["restaurant_id"];
            $randers_box->restaurant_scheme_id = $st;
            $randers_box->restaurant_hall_id = $st;
            $randers_box->order_key = $restorans->id;
            $randers_box->save();
        }
        if ((int)$restorans->order_price > 0) {
            $order = order_generated(
                [
                    "orderNumber" => $restorans->id,
                    "amount" => (int)$restorans->order_price,
                ]
            );
            if (!isset($order["errorCode"])) {
                $restorans->orderpayid = $order["orderId"];
                $restorans->status_pay = "payment";
                $restorans->save();
                return $this->sendResponse(["formUrl" => $order["formUrl"], "status" => "pay"], '');
            } else {
                return $this->sendError(["data" => $order, "status" => "error"]);
            }
        }

        return $this->sendResponse($request, '');
    }

    public function ferialfile(Request $request)
    {
        $request = $request->all();

        $file_type = $request["document"]->getClientOriginalExtension();


        $nca = new NCANodeClient('http://127.0.0.1:14579');
        $p12InBase64 = base64_encode((file_get_contents($request["ecp"]->getRealPath())));
        $document = base64_encode(file_get_contents(($request["document"]->getRealPath())));
        $document_cms = $nca->rawSign($document, $p12InBase64, $request["password"]);
        $text = base64_decode($document_cms["result"]["cms"]);

        $file_name = Str::random(4) . "." . $file_type . ".cms";
        $fp = fopen($file_name, "w");
        fwrite($fp, $text);
        fclose($fp);
        return $this->sendResponse($file_name, '');

    }

    public function checkgile(Request $request)
    {
        $request = $request->all();
        $nca = new NCANodeClient('http://127.0.0.1:14579');
        $document = base64_encode(file_get_contents(($request["document"]->getRealPath())));
        $document_cms = $nca->cmsVerify($document);
        return $this->sendResponse($document_cms["result"], '');
    }

    public function typeRowDb()
    {
        $resoebs = get_type_sql();
        foreach ($resoebs as $index => $ksa) {
            $resoebs[$index] = array_values($ksa);
        }
        return $this->sendResponse($resoebs, '');
    }

    public function delete_point(Request $request)
    {
        $request = $request->all();
        \App\Models\RestaurantScheme::find($request["id"])->delete();
    }

    public function get_point($id)
    {
        $return_items = [];
        $restauranHall = \App\Models\RestaurantScheme::where("restaurant_hall_id", $id)->get();
        foreach ($restauranHall as $item_res) {
            $scheme_element = \App\Models\SchemeElements::find($item_res->scheme_elements_id);
            if (is_null($scheme_element)) {
            } else {

                $scheme_element = $scheme_element->toarray();
                if (!is_null($scheme_element)) {
                    $scheme_element["scheme_elements_id"] = $scheme_element["id"];
                    $scheme_element["offsety"] = $item_res->offsety;
                    $scheme_element["offsetx"] = $item_res->offsetx;
                    $scheme_element["scalex"] = ((float)($item_res->scalex) < 0 || $item_res->scalex == null) ? 1 : $item_res->scalex;
                    $scheme_element["scaley"] = ((float)($item_res->scaley) < 0 || $item_res->scaley == null) ? 1 : $item_res->scaley;
                    $scheme_element["name"] = $item_res->name;
                    $scheme_element["rotation"] = $item_res->rotation;
                    $scheme_element["angle"] = $item_res->angle;
                    $scheme_element["id"] = $item_res->id;
                    array_push($return_items, $scheme_element);
                }
            }
        }
        return $this->sendResponse($return_items, '');
    }


    public function user_update(Request $request)
    {

        $this->users = auth()->guard('api')->user();
        if (empty($this->users)) {
            return $this->sendError('вы не авторизованы');
        }

        $user_save = \App\Models\User::find($this->users->id);
        $user_save->name = $request->name;
        $user_save->save();


    }

    public function update_point(Request $request)
    {
        $request = $request->all();
        $restauranHall = \App\Models\RestaurantScheme::find($request["id"]);
        if (!is_null($restauranHall)) {
            $restauranHall->offsety = $request["offsety"];
            $restauranHall->offsetx = $request["offsetx"];

            if (isset($request["angle"])) {
                $restauranHall->angle = $request["angle"];
            }

            if (isset($request["scaleY"])) {
                $restauranHall->scaley = $request["scaleY"];
            }
            if (isset($request["scaleX"])) {
                $restauranHall->scalex = $request["scaleX"];
            }
            $restauranHall->save();
        }

    }

    public function update_size(Request $request)
    {
        $request = $request->all();
        $restauranHall = \App\Models\RestaurantScheme::find($request["id"]);
        if (!is_null($restauranHall)) {
            $restauranHall->scaley = $request["scaley"];
            $restauranHall->scalex = $request["scalex"];
            $restauranHall->save();
        }

    }

    public function add_point(Request $request)
    {
        $request = $request->all();
        $restauranHall = new \App\Models\RestaurantScheme;
        $restauranHall->restaurant_hall_id = $request["restaurant_hall_id"];
        $restauranHall->scheme_elements_id = $request["scheme_elements_id"];
        $restauranHall->offsety = $request["offsety"];
        $restauranHall->offsetx = $request["offsetx"];

        if (isset($request["scaleX"])) {
            $restauranHall->scalex = $request["scaleX"];
        }
        if (isset($request["scaleY"])) {
            $restauranHall->scaley = $request["scaleY"];
        }

        $restauranHall->name = $request["name"];
        $restauranHall->rotation = 0;
        $restauranHall->save();
    }

    public function menu_search(Request $request)
    {
        $request = $request->all();

        $restoran_maenu = \App\Models\RestaurantMenu::where("name", "LIKE", "%" . $request["search"] . "%")->get()->groupby("restaurant_id")->toarray();
        $restoran = \App\Models\Restaurant::where("status", "!=", "0")->get()->groupby("id")->toarray();

        $return_menu = [];
        foreach ($restoran_maenu as $index => $resbox) {
            if (isset($restoran[$index])) {
                array_splice($resbox, 9);
                array_push($return_menu, [$restoran[$index][0], $resbox]);
            }
        }

        $response = [
            'success' => true,
            'data' => $return_menu,
            'message' => "",
        ];


        return response()->json($response, 200);

    }

    public function reserved(Request $request)
    {

        $restorans = \App\Models\Restaurant::find($request["id"]);

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);

        $save_from = $from->format('Y-m-d');
        $select_date = $from->format('Y-m-d H:i:s');

        $timesx = (int)date("H", strtotime($request->to));
        if ($timesx > 13 || ($timesx > 0 && $timesx < 9)) {
            if (!is_null($restorans->night_landing)) {
                if ((int)$restorans->night_landing > 0) {
                    $from = $from->subHours((int)$restorans->night_landing);
                    $to = $to->addHour((int)$restorans->night_landing);
                }
            }
        } else {
            if (!is_null($restorans->day_landing)) {
                if ((int)$restorans->day_landing > 0) {
                    $from = $from->subHours((int)$restorans->day_landing);
                    $to = $to->addHour((int)$restorans->day_landing);
                }
            }
        }

        $from = $from->format('Y-m-d H:i:s');
        $to = $to->format('Y-m-d H:i:s');

        $reservation = \App\Models\Reservation::where(function ($query) use ($request, $from, $to) {
            $query->where(function ($query2) use ($request, $from, $to) {
                $query2->where("date_from", "<=", $from)
                    ->where("date_to", ">=", $from);
            })->Orwhere(function ($query2) use ($request, $to) {
                $query2->where("date_from", "<=", $to)
                    ->where("date_to", ">=", $to);
            });
        })->Orwhere(function ($query) use ($request, $from, $to) {
            $query->where(function ($query2) use ($request, $from, $to) {
                $query2->where("date_from", ">=", $from)
                    ->where("date_from", "<=", $to);
            })->Orwhere(function ($query2) use ($request, $from, $to) {
                $query2->where("date_to", ">=", $from)
                    ->where("date_to", "<=", $to);
            });
        })->where("restaurant_id", $restorans->id)->get()->toarray();

        $start_dy = strtotime($save_from . " " . $restorans->schedule_from);
        $startWorkToDay = strtotime(date('Y-m-d', strtotime('+1 day', strtotime($save_from))) . " " . $restorans->schedule_from);


        $endWorkCurrentDay = strtotime($save_from . " " . $restorans->schedule_to);
        $endWorkToDay = strtotime(date('Y-m-d', strtotime('+1 day', strtotime($save_from))) . " " . $restorans->schedule_to);


        $reserv = false;
        if (($endWorkToDay - $startWorkToDay) < 0) {
            if (strtotime($select_date) < $start_dy && strtotime($select_date) > $endWorkCurrentDay) {
                $reserv = true;
            }
        } else {
            if (strtotime($select_date) < $start_dy || strtotime($select_date) > $endWorkCurrentDay) {
                $reserv = true;
            }
        }

        if ($reserv) {

            $reserv = \App\Models\RestaurantHall::where("restaurant_id", $restorans->id)->get();
            foreach ($reserv as $reserv) {
                foreach (\App\Models\RestaurantScheme::where("restaurant_hall_id", $reserv->id)->get() as $ids => $resbons) {
                    array_push($reservation, [
                        "created_at" => "2021-05-30T15:53:22.000000Z",
                        "date_from" => $from,
                        "date_to" => $to,
                        "id" => $ids,
                        "name" => "Не работает",
                        "order_key" => "",
                        "restaurant_hall_id" => 0,
                        "restaurant_id" => $restorans->id,
                        "restaurant_scheme_id" => $resbons->id,
                        "status" => "receiving",
                        "tags" => null,
                        "tel" => "",
                        "updated_at" => "",
                        "user_id" => null,
                    ]);
                }
            }
        }

        return $this->sendResponse($reservation, '');
    }

    public function reserved_save(Request $request)
    {
        $request = $request->all();
        $stol = explode(",", $request["stol"]);

        $restorans = new \App\Models\ReservationOrder;
        $restorans->desk = implode(",", $stol);
        $restorans->name = $request["name"];
        $restorans->tel = validate_phone_number($request["tel"]);
        $restorans->person = $request["person"];
        $restorans->status_pay = "done";
        $restorans->status = "confirmation";
        $restorans->order_price = "0";
        $us_user = \App\Models\User::where("tel", validate_phone_number($request["tel"]))->first();
        if (!is_null($us_user)) {
            $restorans->user_id = $us_user->id;
        }
        $restorans->restaurant_id = $request["restaurant_id"];
        $restorans->date = date('Y-m-d H:i:s', strtotime($request["from"]));
        $restorans->save();

        foreach ($stol as $st) {
            $randers_box = new \App\Models\Reservation;
            $randers_box->date_from = Carbon::parse($request["from"])->format('Y-m-d H:i:s');
            $randers_box->date_to = Carbon::parse($request["to"])->format('Y-m-d H:i:s');
            $randers_box->name = $request["name"];
            $randers_box->tel = validate_phone_number($request["tel"]);
            $randers_box->restaurant_id = $request["restaurant_id"];
            $randers_box->restaurant_scheme_id = $st;
            $randers_box->restaurant_hall_id = $request["hall_id"];
            $randers_box->order_key = $restorans->id;
            $randers_box->save();
        }


        foreach (\App\Models\User::where("restaurant_id", $request["restaurant_id"])->get() as $user_res) {
            $tokens_id = \App\Models\TokenList::where("user_id", $user_res->id)->get();
            if (count($tokens_id) > 0) {
                foreach ($tokens_id as $tokensx) {
                    push([
                        "to" => $tokensx->token,
                        "title" => "Подана заявка на бронь ",
                        "subtitle" => "Заявка на столы:" . $restorans->desk . " ",
                        "body" => "Заявка на столы:" . $restorans->desk . " ",
                    ], $user_res->id);
                }
            } else {
                push([
                    "to" => "/topics/User_" . $user_res->id,
                    "title" => "Подана заявка на бронь ",
                    "subtitle" => "Заявка на столы:" . $restorans->desk . " ",
                    "body" => "Заявка на столы:" . $restorans->desk . " ",
                ], $user_res->id);
            }
        }

        if (!is_null($us_user)) {
            push([
                "to" => "/topics/User_" . $us_user->id,
                "title" => "Вы подали заявку на бронь в " . $restorans->restaurant_id,
                "subtitle" => "Заявка на " . $restorans->desk . " ",
                "body" => "Заявка на " . $restorans->desk . " ",
            ], $us_user->id);
        } else {
            if (validate_phone_number($request["tel"]) !== false) {

                $data = array(
                    'login' => "TeamGis",
                    'psw' => "gIs2021",
                    'phones' => '7' . validate_phone_number($request["tel"]),
                    'mes' => "Имя " . $restorans->name . " благодарим Вас за выбор нашего заведения. мы ждем Вас в ресторане/кафе " . $restorans->restaurant_id . " " . $restorans->date . ". Будем рады видеть Вас и для Вашего удобства в будущем рекомендуем скачать приложение http://onelink.to/d5py96",
                );
                $info = file_get_contents('https://smsc.kz/sys/send.php?' . http_build_query($data));
            }
        }


    }

}
