<?php

namespace App\Http\Controllers\Api\Admin\Constructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use TableClass;

class TableController extends BaseController
{
    protected $users;


    public function __construct(Request $request)
    {
        $this->users = auth()->guard('api')->user();

//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }


    public function typeRowDb()
    {

        return $this->sendResponse(get_type_sql(), '');
    }


    public function update(Request $request)
    {
        $request = $request->all();


        $table = new TableClass($request["table"], $request["row"]);
        $table->save();

        foreach ($request["row"] as $index => $tba) {
            $request["row"][$index]["nameOld"] = $tba["name"];
        }

        return $this->sendResponse($request["row"], '');
    }

    public function delete(Request $request)
    {
        $request = $request->all();
        $model_list = file_get_contents("../database/db_list.json");
        $model_list = json_decode($model_list, true);
        $model_list_save = $model_list;
        foreach ($model_list as $index => $table) {
            if ($table["name"] == $request["name"]) {
                $table = new TableClass(["model" => $table["model"]], []);
                unset($model_list_save[$index]);
                sort($model_list_save);
                $table->remove($model_list_save);
            }
        }
//        dd($model_list);
        return $this->sendResponse($model_list, '');
    }

    public function table_list()
    {
        $model_list = file_get_contents("../database/db_list.json");
        $model_list = json_decode($model_list, true);
        return $this->sendResponse($model_list, '');
    }

    public function table_list_model($model)
    {
        $model_list = file_get_contents("../database/db_list.json");
        $model_list = json_decode($model_list, true);
        foreach ($model_list as $t_old) {
            if ($t_old["name"] == $model) {
                $model_row = file_get_contents("../database/json/" . $t_old["table"] . ".json");
                $model_row = json_decode($model_row, true);

                if (file_exists("../database/json/bread/" . $t_old["table"] . ".json")) {

                    $model_row_sort = file_get_contents("../database/json/bread/" . $t_old["table"] . ".json");
                    $model_row_sort = json_decode($model_row_sort, true);
                    $model_row = sortByPredefinedOrder($model_row, $model_row_sort);

                }

                return $this->sendResponse(["table" => $t_old, "row" => $model_row], '');
            }
        }
    }


}
