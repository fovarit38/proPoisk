<?php

namespace App\Http\Controllers\Api\Admin\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use TableClass;
use Storage;
use Str;
use NCANodeClient;
use Illuminate\Support\Facades\Hash;
use Schema;
use Carbon\Carbon;
use App\Imports\RestaurantMenuImport;
use Maatwebsite\Excel\Facades\Excel;

class MainController extends BaseController
{
    protected $users;


    public function __construct(Request $request)
    {
//        $this->users = auth()->guard('api')->user();
//
//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }


    public function edit_column_get($modul, $id)
    {

        if (!file_exists("../app/Models/" . $modul . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model = app("\App\\Models\\" . $modul);


        $table = $model->getTable();
        $crow = "add";
        if ($id != "0") {
            $model = $model::find($id);
            if (!is_null($model)) {
                $crow = "edit";
            }
        }


        if (!file_exists("../database/json/bread/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }
        if (!file_exists("../database/json/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }

        $model_list = file_get_contents("../database/json/bread/" . $table . ".json");
        $model_list = json_decode($model_list, true);

        $request = file_get_contents("../database/json/" . $table . ".json");
        $request = json_decode($request, true);

        $thead_return = [];

        foreach ($model_list as $name => $thead) {
            if (isset($thead["views"])) {
                if (in_array($crow, $thead["views"])) {
                    $model_has_one = [];
                    if ($thead["type"] == "Image") {
                        $thead["type"] = "file";
                    } else if ($thead["type"] == "hasOne") {
                        $model_has_one = app("\App\\Models\\" . $thead["hasone"]);
                        $model_has_one = $model_has_one->get()->toarray();
                        if ($thead["hasone"] == "RestaurantScheme") {
                            foreach ($model_has_one as $index => $lms) {

                                $hall = \App\Models\RestaurantHall::find($lms["restaurant_hall_id"]);
                                if (!is_null($hall)) {
                                    $restoran = \App\Models\Restaurant::first();
//                                    \App\Models\Restaurant::
                                    $model_has_one[$index]["name"] = $restoran->name . " / " . $hall->name . " / " . $lms["name"];
                                }
                            }
                        }
                    }

                    $input_info = array_filter($request, function ($value) use ($name) {
                        return $value['name'] == $name;
                    });
                    sort($input_info);
                    if (isset($input_info[0])) {

                        $input_info = $input_info[0];


                        if (isset($thead["display"]) ? (str_replace(" ", "", $thead["display"]) != "" ?: false) : false) {

                            array_push($thead_return, ["name" => $name, "lang" => ((isset($input_info["lang"]) && $input_info["lang"] == "1")), "placeholder" => $thead["display"], "type" => $thead["type"], "hasOne" => $model_has_one, "required" => (isset($input_info["notnull"]) ? true : false)]);

                        } else {
                            array_push($thead_return, ["name" => $name, "placeholder" => $name, "type" => $thead["type"], "hasOne" => $model_has_one, "required" => (isset($input_info["notnull"]) ? true : false)]);
                        }
                    }

                }
            }
        }
        return $this->sendResponse($thead_return, '');
    }

    public function browse_column_get($modul)
    {
        if (!file_exists("../app/Models/" . $modul . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model = app("\App\\Models\\" . $modul);
        $table = $model->getTable();

        if (!file_exists("../database/json/bread/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }

        $model_list = file_get_contents("../database/json/bread/" . $table . ".json");
        $model_list = json_decode($model_list, true);


        $thead_return = [];
        foreach ($model_list as $name => $thead) {
            if (isset($thead["views"])) {
                if (in_array("browse", $thead["views"])) {
                    if (isset($thead["display"]) ? (str_replace(" ", "", $thead["display"]) != "" ?: false) : false) {
                        $thead_return[$name] = $thead["display"];
                    } else {
                        $thead_return[$name] = $name;
                    }
                }
            }
        }
        return $this->sendResponse($thead_return, '');
    }

    public function database_single_get($model, $id)
    {

        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        $model = $model->find($id);

        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }

        return $this->sendResponse($model, '');
    }

    public function browse_delete($model, Request $request)
    {

        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        $model = $model->find($request["id"]);
        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }
        if ($model_name == "RestaurantHall") {
            $re_shema = \App\Models\SchemeElements::where("restaurant_id", $model->id)->get();
            foreach ($re_shema as $rs_s) {
                $rs_s->delete();
            }
        }

        $model->delete();
        return $this->sendResponse($model, '');
    }

    public function database_all_get($model, Request $request)
    {
        $request = $request->all();
        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        if (isset($request["restaurant_id"])) {
            if ($model_name == "SchemeElements") {
                $model = $model->wherein("restaurant_id", ["all", $request["restaurant_id"]]);
            } else {
                $model = $model->where("restaurant_id", $request["restaurant_id"]);
            }
        }

        if (isset($request["catalog_id"])) {
            if ($model_name != "Restaurant") {
                $model = $model->where("catalog_id", $request["catalog_id"]);
            } else {
                $list_array = \App\Models\CatalogItem::where("catalog_id", $request["catalog_id"])->get()->groupby("restaurant_id")->toarray();
                $list_array = array_keys($list_array);
                $model = $model->wherein("id", $list_array);
            }
        }
        if (isset($request["filter"])) {
            $filter = json_decode($request["filter"], true);
            if (!is_null($filter)) {
                foreach ($filter as $key => $filr) {
                    if ($key == "price") {
                        if (isset($filr["to"])) {
                            $model = $model->where($key, "<=", (int)$filr["to"]);
                        }
                        if (isset($filr["from"])) {
                            $model = $model->where($key, ">=", (int)$filr["from"]);
                        }
                    } else {
                        if (strpos($key, '_ch') !== false) {
                            if ($filr == "1") {
                                $model = $model->where($key, $filr);
                            }
                        } else {
                            $model = $model->where($key, $filr);
                        }

                    }
                }
            }
        }
        if (isset($request["user_id"])) {
            $model = $model->where("user_id", $request["user_id"]);
        }
        if (isset($request["status"]) && $model_name != "Catalog") {
            $model = $model->where("status", $request["status"]);
        }
        if (isset($request["type"]) && $model_name != "Catalog") {
            $model = $model->where("type", $request["type"]);
        }

        $model = $model->get();


        if ($model_name == "Catalog") {
            if(!isset($_REQUEST["hide"])){
                $model = $model->whereNull("catalog_id")->toarray();
            }

            foreach ($model as $index => $mo) {
                $rest_box = \App\Models\CatalogItem::where("catalog_id", $mo["id"])->get();
                $chiledren = \App\Models\Catalog::where("catalog_id", $mo["id"])->get();
                $array_keys = array_keys($rest_box->groupby("restaurant_id")->toarray());
                $preSql = \App\Models\Restaurant::wherein("id", $array_keys);
                if (isset($request["type"])) {
                    $preSql->where("type", $request["type"]);
                }
                if (isset($request["status"])) {
                    $preSql->where("status", $request["status"]);
                }
                $model[$index]["counts"] = $preSql->count();
                $model[$index]["child"] = $chiledren;
            }
        }
        if ($model_name == "Restaurant") {
            $model = $model->toarray();
            foreach ($model as $index => $mo) {
                $idlist = array_keys(\App\Models\CatalogItem::where("restaurant_id", $mo["id"])->get()->groupby("catalog_id")->toarray());
                $chatList = array_keys(\App\Models\Chat::where("chat_id", $mo["id"])->where("user_from", "!=", $idlist)->get()->groupby("user_from")->toarray());
                if (!is_null($idlist)) {
                    $model[$index]["catalog_id"] = implode(",", $idlist);
                }
                $model[$index]["chat_open"] = count($chatList);

            }
        }
        $all_count = count($model);
        $counts = 100;

        if (isset($_REQUEST["page"])) {
//            $model = $model->skip(($_REQUEST["page"] - 1) * $counts)->take($counts);
        }

        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }
        return $this->sendResponse($model, ["all" => $all_count, "pages" => ceil($all_count / $counts)]);
    }

    public function database_list()
    {
        $model_list = json_decode(file_get_contents("../database/db_list.json"), true);
        $models = [];
        foreach ($model_list as $model) {
            array_push($models, $model["name"]);
        }
        return $this->sendResponse($models, '');
    }

    public function database_update(Request $request)
    {
        $files = $request;
        $request = $request->all();

        if (!file_exists("../app/Models/" . $request["model_name"] . ".php")) {
            return $this->sendResponse([], 'error');
        }


        $model_list = array_filter(json_decode(file_get_contents("../database/db_list.json"), true), function ($value) use ($request) {
            return $value['model'] == $request["model_name"];
        });
        sort($model_list);

        $save_date = [];
        $model_row = [];
        if (isset($model_list[0])) {
            $model_list = $model_list[0];
            if (file_exists("../database/json/bread/" . $model_list["table"] . ".json")) {
                $model_row = file_get_contents("../database/json/bread/" . $model_list["table"] . ".json");
                $model_row = json_decode($model_row, true);
            }
        }

        $id = $request["id"];
        $model_name = $request["model_name"];
        $model = null;
        $brif_update = false;


        if ($model_name == "Message") {
            push([
                "to" => "/topics/all",
                "title" => $request["save"]["name"],
                "subtitle" => $request["save"]["name"],
                "body" => $request["save"]["message"],
            ]);
        }


        if ($id == 0) {
            $model = app("\App\Models\\$model_name");
            if ($model_name == "Restaurant" && $id == 0) {
                $brif_update = true;
            }
        } else {
            $model = app("\App\Models\\$model_name");
            $model = $model->find($id);
        }

        if (method_exists($model, 'preprocessing')) {
            $request = $model->preprocessing($request);
        }

        if (isset($_FILES["save"])) {
            foreach ($_FILES["save"]["name"] as $nameInput => $fileout) {
                if ($files->hasFile("save." . $nameInput)) {
                    $filename = Str::random(6) . "." . $files["save"][$nameInput]->getClientOriginalExtension();
                    $files_is = Storage::disk('uploads')->put($filename, file_get_contents($files["save"][$nameInput]->getRealPath()));
                    if ($files_is) {
                        $request["save"][$nameInput] = "/storage/uploads/" . $filename;
                    } else {
                        unset($request["save"][$nameInput]);
                    }
                } else {
                    unset($request["save"][$nameInput]);
                }
            }
        }

        foreach ($request["save"] as $key => $input) {
            if (is_array($input)) {
                $model->{$key} = json_encode($input, JSON_UNESCAPED_UNICODE);
            } else {

                $type_save = isset($model_row[$key]) ? $model_row[$key] : "string";
                if ($type_save["type"] == "Password") {
                    if (!empty($input)) {
                        $model->{$key} = Hash::make($input);
                    }
                } else {
                    $model->{$key} = $input;
                }
            }
        }


        $model->save();


        if ($brif_update) {

            $briaf_update = [
                'address',
                'Installment',
                'Price',
            ];
            foreach ($briaf_update as $bbs) {
                $brif = new \App\Models\Brief;
                $brif->icon = $bbs;
                $brif->content = "";
                $brif->restaurant_id = $model->id;
                $brif->save();
            }
        }

        return $this->sendResponse([$model], '');
    }


}
