<?php

namespace App\Http\Controllers\Api\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Storage;

class MainController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Товары в корзине
    protected $users;


    public function __construct(Request $request)
    {

    }

    public function setChat(Request $request)
    {
        $chat = new \App\Models\Chat;
        $chat->user_to = $request->user_to;
        $chat->user_from = $request->user_from;
        $chat->message = $request->message;
        $chat->type = $request->type;
        $chat->chat_id = $request->chat_id;
        $chat->save();

        $users = \App\Models\User::find($request->user_from);
        push([
            "to" => "/topics/User_" . $request->user_to,
            "title" => "" . $users->name,
            "body" => $request->message,
        ]);
    }

    public function set_online($id)
    {
//        $users = \App\Models\User::find($id);
//        $users->online = date('Y-m-d H:i:s');
//        $users->save();
    }

    public function getChatSingle($id, Request $request)
    {
        $advInfo = \App\Models\Restaurant::find($id);
        $chat = \App\Models\Chat::where("chat_id", $id)->get()->groupBy("user_from")->toarray();
        $result = [];
        foreach ($chat as $user_key => $us) {
            if ($user_key != $advInfo->user_id) {
                $user_date = \App\Models\User::find($user_key);
                $last = end($us);
                if (!is_null($user_date)) {
                    array_push($result, ["user" => $user_date, "counts" => count($us), "reviews" => 2, "last" => $last]);
                }
            }
        }
        return $this->sendResponse($result, 'Чат');
    }


    public function get_user_count($id)
    {
        $count = \App\Models\Chat::where("user_to", $id)->get()->count();

        return $this->sendResponse($count, 'Чат');
    }

    public function update_file(Request $request)
    {
        $request = $request->all();
        $files = [];
        foreach ($request["files"] as $file) {
            $base64_str = $file["base64"];
            $image = base64_decode($base64_str);

            $name = Str::random(10) . '.jpg';
            Storage::disk('uploads')->put($name, $image);
            array_push($files, "/storage/uploads/" . $name);
        }
        return $this->sendResponse($files, 'Загрузка картинок произошла');
    }


    public function getChatClient(Request $request)
    {
//        $chat = \App\Models\Chat::where("user_from", )->where("user_to", )->get();

        $ignores = array_keys(\App\Models\Restaurant::where("user_id", $request->user_id)->get()->groupBy("id")->toarray());

        $chat = array_keys(\App\Models\Chat::whereNotIn("chat_id", $ignores)
            ->where(function ($query) use ($request) {
                $query->where('user_to', $request->user_id)
                    ->Orwhere('user_from', $request->user_id);
            })
            ->get()->groupBy("chat_id")->toarray());

        $restArray = \App\Models\Restaurant::wherein("id", $chat)->get()->toarray();

        foreach ($restArray as $index => $resa) {
            $chatNews = \App\Models\Chat::where("chat_id", $resa["id"])->orderby("id", "desc")->get()->toarray();
            $restArray[$index]["chat"] = ["message" => $chatNews[0]["message"], "created_at" => $chatNews[0]["created_at"]];
        }

        return $this->sendResponse($restArray, 'Чат');
    }

    public function getChat(Request $request)
    {
//        $chat = \App\Models\Chat::where("user_from", )->where("user_to", )->get();

        $chat = \App\Models\Chat::where(function ($query) use ($request) {


            $query->where(function ($query2) use ($request) {
                $query2->where('user_from', $request->user_from)
                    ->where('user_to', $request->user_to);
            })->Orwhere(function ($query2) use ($request) {
                $query2->where('user_to', $request->user_from)
                    ->where('user_from', $request->user_to);
            });
        })->where("chat_id", $request->chat_id)->get();

        return $this->sendResponse($chat, 'Чат');
    }

    public function statys_update_single($id, $status)
    {
        $rest = \App\Models\Restaurant::find($id);
        $rest->status = $status;
        $rest->save();
    }

    public function statys_update()
    {
        $rest = \App\Models\Restaurant::where("status", 1)->get();
        foreach ($rest as $st) {
            $st->status = 2;
            $st->save();
        }
    }

    public function advCreate(Request $request)
    {

        $adv = new \App\Models\Restaurant;
        if (isset($request->name)) {
            $adv->name = $request->name;
        }
        if (isset($request->content)) {
            $adv->content = $request->content;
        }
        if (isset($request->images)) {
            $adv->images = implode(',',$request->images);
        }
        if (isset($request->free_installation_ch)) {
            $adv->free_installation_ch = ($request->free_installation_ch);
        }

        if (isset($request->guarantee_ch)) {
            $adv->guarantee_ch = ($request->guarantee_ch);
        }
        if (isset($request->guarantee)) {
            $adv->guarantee = ($request->guarantee);
            if ((int)($request->guarantee) > 0) {
                $adv->guarantee_ch = 1;
            } else {
                $adv->guarantee_ch = 0;
            }
        }

        if (isset($request->Installment_ch)) {
            $adv->Installment_ch = ($request->Installment_ch);
        }
        if (isset($request->installment)) {
            $adv->installment = ($request->installment);
            if ((int)($request->installment) > 0) {
                $adv->Installment_ch = 1;
            } else {
                $adv->Installment_ch = 0;
            }
        }


        if (isset($request->free_shipping_ch)) {
            $adv->free_shipping_ch = ($request->free_shipping_ch);
        }
        if (isset($request->free_installation_ch)) {
            $adv->free_installation_ch = ($request->free_installation_ch);
        }
        if (isset($request->production)) {
            $adv->production = ($request->production);
        }

        if (isset($request->price)) {
            $adv->price = ($request->price);
        }
        if (isset($request->city_id)) {
            $adv->city_id = ($request->city_id);
        }
        if (isset($request->user_id)) {
            $adv->user_id = ($request->user_id);
        }
        if (isset($request->type)) {
            $adv->type = ($request->type);
        }
        $adv->save();

        if (isset($request->catalog)) {
            \App\Models\CatalogItem::where("restaurant_id", $adv->id)->delete();
            foreach ($request->catalog as $cat) {
                $catalog = new \App\Models\CatalogItem;
                $catalog->restaurant_id = $adv->id;
                $catalog->catalog_id = $cat;
                $catalog->save();
            }
        }

        return $this->sendResponse($adv, 'Создан');
    }

    public function advUpdate($id, Request $request)
    {
        $adv = \App\Models\Restaurant::find($id);
        if (is_null($adv)) {
            return $this->sendError([], 'error');
        }
        if (isset($request->name)) {
            $adv->name = $request->name;
        }
        if (isset($request->content)) {
            $adv->content = $request->content;
        }
        if (isset($request->images)) {
            $adv->images = implode(',',$request->images);
        }


        if (isset($request->free_installation_ch)) {
            $adv->free_installation_ch = ($request->free_installation_ch);
        }

        if (isset($request->guarantee_ch)) {
            $adv->guarantee_ch = ($request->guarantee_ch);
        }
        if (isset($request->production)) {
            $adv->production = ($request->production);
        }

        if (isset($request->guarantee)) {
            $adv->guarantee = ($request->guarantee);
            if ((int)($request->guarantee) > 0) {
                $adv->guarantee_ch = 1;
            } else {
                $adv->guarantee_ch = 0;
            }
        }

        if (isset($request->Installment_ch)) {
            $adv->Installment_ch = ($request->Installment_ch);
        }
        if (isset($request->installment)) {
            $adv->installment = ($request->installment);
            if ((int)($request->installment) > 0) {
                $adv->Installment_ch = 1;
            } else {
                $adv->Installment_ch = 0;
            }
        }
        if (isset($request->free_shipping_ch)) {
            $adv->free_shipping_ch = ($request->free_shipping_ch);
        }
        if (isset($request->free_installation_ch)) {
            $adv->free_installation_ch = ($request->free_installation_ch);
        }

        if (isset($request->price)) {
            $adv->price = ($request->price);
        }
        if (isset($request->city_id)) {
            $adv->city_id = ($request->city_id);
        }
        if (isset($request->user_id)) {
            $adv->user_id = ($request->user_id);
        }
        if (isset($request->type)) {
            $adv->type = ($request->type);
        }


        $adv->save();
        if (isset($request->catalog)) {
            \App\Models\CatalogItem::where("restaurant_id", $adv->id)->delete();
            foreach ($request->catalog as $cat) {
                $catalog = new \App\Models\CatalogItem;
                $catalog->restaurant_id = $adv->id;
                $catalog->catalog_id = $cat;
                $catalog->save();
            }
        }

        return $this->sendResponse($adv, 'Обновлен');
    }
}
