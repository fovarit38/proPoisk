<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchemeElements extends Model
{
    use HasFactory;

    public function getIconAttribute($value)
    {
        return "/public".$value;
    }
    public function getIconbookedAttribute($value)
    {
        return "/public".$value;
    }
}
