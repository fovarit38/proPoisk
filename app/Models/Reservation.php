<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;


    public function getDateToAttribute($value)
    {
        return date('Y-m-d\TH:i', strtotime($value));
    }
    public function getDateFromAttribute($value)
    {
        return date('Y-m-d\TH:i', strtotime($value));
    }


}
