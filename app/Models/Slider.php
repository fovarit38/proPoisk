<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;
    public function getImagesAttribute($value)
    {
        return str_replace("\/","/public/",str_replace(public_path(),"",resize(public_path($value), 720)));
    }
}
