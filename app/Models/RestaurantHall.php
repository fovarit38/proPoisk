<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RestaurantHall extends Model
{
    use HasFactory;
    public function getSchemaAttribute($value)
    {
        return "/public".$value;
    }
}
