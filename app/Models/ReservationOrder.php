<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationOrder extends Model
{
    use HasFactory;

    public function getDateToAttribute($value)
    {
        return date('Y-m-d\TH:i', strtotime($value));
    }

    public function getControlAttribute($value)
    {
        if($this->status=="Отклонена" || $this->status=="Завершена"){
            return "";
        }
        return $value;
    }

    public function getDeskAttribute($value)
    {
        return implode(";\n ", array_keys(\App\Models\RestaurantScheme::wherein("id", explode(",", $value))->get()->groupby("name")->toarray()));
    }


    public function getStatusAttribute($value)
    {
        $status_key = [
            "new" => "На рассмотрении",
            "confirmation" => "Подтверждена",
            "Сompleted" => "Завершена",
            "Reject" => "Отклонена",
        ];
        if (isset($status_key[$value])) {
            return $status_key[$value];
        }
        return $value;
    }

    public function getRestaurantIdAttribute($value)
    {
        $res_name = \App\Models\Restaurant::find($value);

        if (!is_null($res_name)) {
            return $res_name->name;
        }

        return $value;
    }
}
