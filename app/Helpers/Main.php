<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


function validate_phone_number($phone)
{
    $phone = str_replace("-", "", filter_var($phone, FILTER_SANITIZE_NUMBER_INT));

    if (mb_substr($phone, 0, 1) == "8") {
        $phone = mb_substr($phone, 1, mb_strlen($phone));
    } else if (mb_substr($phone, 0, 1) == "+") {
        $phone = mb_substr($phone, 2, mb_strlen($phone));
    } else if (mb_substr($phone, 0, 1) == "7"  && strlen($phone)==11) {
        $phone = mb_substr($phone, 1, mb_strlen($phone));
    }


    $phone = preg_replace("/[^0-9]/", '', $phone);

    if (strlen($phone) != 10) {
        return false;
    } else {
        return $phone;
    }
}


function push($data, $user_id = null)
{

    $url = "https://fcm.googleapis.com/fcm/send";
    $android = [
        "to" => "",
        "notification" => [
            "title" => "",
            "subtitle" => "",
            "body" => "",
            "icon" => "ic_launcher",
            "badge" => 0,
            "high_priority" => "high",
            "show_in_foreground" => true,
            "sound" => "default"
        ],
    ];
    foreach ($data as $key => $attr) {
        if ($key == "to") {
            $android[$key] = $attr;
        } else {
            $android["notification"][$key] = $attr;
        }
    }
    $fields_string = json_encode($android);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Authorization: key=AAAAxNfb0Wk:APA91bFr1LL_Q5GTQYzn6DXi0mk9X8Dn6YiDo6TA-aLbRUgYwjgvlYXosH2r6R9zOl_UuUWqdjN0H40Ux-6besNvAJqzeGyujxNuBl4ktUKbTe6lBbm-NHpvMWNsALPbNHRQsZgw7SUI'
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);

    $push_save = new \App\Models\Push;
    $push_save->to = $android["to"];
    $push_save->title = $android["notification"]["title"];
    $push_save->subtitle = $android["notification"]["subtitle"];
    $push_save->body = $android["notification"]["body"];
    $push_save->icon = $android["notification"]["icon"];
    $push_save->badge = $android["notification"]["badge"];
    $push_save->high_priority = $android["notification"]["high_priority"];
    $push_save->show_in_foreground = $android["notification"]["show_in_foreground"];
    $push_save->sound = $android["notification"]["sound"];
    if (!is_null($user_id)) {
        $push_save->user_id = $user_id;
    }
    $push_save->save();

    return $result;
}

//    $vars['userName'] = 'Teamgis-api';
//    $vars['password'] = 'Teamgis';
//    $cart = array(
//        array(
//            'positionId' => 1,
//            'name' => 'Название товара',
//            'quantity' => array(
//                'value' => 1,
//                'measure' => 'шт'
//            ),
//            'itemAmount' => 1 * (1000 * 100),
//            'itemCode' => '123456',
//            'tax' => array(
//                'taxType' => 0,
//                'taxSum' => 0
//            ),
//            'itemPrice' => 1000 * 100,
//        )
//    );
//    $vars['orderBundle'] = json_encode(
//        array(
//            'cartItems' => array(
//                'items' => $cart
//            )
//        ),
//        JSON_UNESCAPED_UNICODE
//    );

function order_generated($data, $url_Def = 'https://securepayments.sberbank.kz/payment/rest/register.do?')
{

    $vars = array();

    $vars['userName'] = 'teamgis1-api';
    $vars['password'] = 'K22212!!k';
//    $vars['orderNumber'] = $data["orderNumber"];
    foreach ($data as $_ks => $_kv) {
        $vars[$_ks] = $_kv;
    }


    if (isset($data["amount"])) {
        $vars['amount'] = $data["amount"] * 100;
    }


    if (isset($data["amount"])) {
        $vars['returnUrl'] = 'https://teamgis.a-lux.dev/api/success';
        $vars['failUrl'] = 'https://teamgis.a-lux.dev/api/error';
        $vars['description'] = 'Заказ №' . $data["orderNumber"] . ' на example.com';
    }

    $ch = curl_init($url_Def . http_build_query($vars));
//    $ch = curl_init('https://3dsec.sberbank.kz/payment/rest/register.do?' . http_build_query($vars));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $res = curl_exec($ch);
    curl_close($ch);

    return json_decode($res, true);
}

function findNearestRestaurants($longitude, $latitude, $radius = 400)
{
    $restaurants = \App\Models\Restaurant::selectRaw("* ,
                         ( 6371000 * acos( cos( radians(?) ) *
                           cos( radians( latitude ) )
                           * cos( radians( longitude ) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( latitude ) ) )
                         ) AS distance", [$latitude, $longitude, $latitude])
        ->having("distance", "<", $radius)
        ->orderBy("distance", 'asc')
        ->get();

    return $restaurants;
}


function resize($path, $size)
{

    $format = explode(",", 'jpg,jpeg,png,jpg,gif,svg');

    $new_path = str_replace("/uploads/", "/uploads/w" . $size . "/", $path);

    if (file_exists($path) && !file_exists($new_path)) {
        $name = explode(".", $path);
        $name_origin = explode("/", $path);
        $name_origin = end($name_origin);
        $types = end($name);
        if (!in_array($types, $format) || is_null($path)) {
            return "";
        }
        $imagename = time() . '.' . $types;
        $destinationPath = public_path('/thumbnail');
        $img = Image::make(file_get_contents($path));
        $img->resize($size, $size, function ($constraint) {
            $constraint->aspectRatio();
        })->encode($types);
        Storage::disk('uploads')->put("/w" . $size . "/" . $name_origin, $img->__toString());
    }

    return $new_path;

}

function is_admin_view()
{

    if (isset($_SERVER['REQUEST_URI'])) {
        $adminType = explode("?", $_SERVER['REQUEST_URI']);
        $adminType = explode("/", $adminType[0]);

        if (in_array("admin", $adminType)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function url_routes()
{

    return '';
}

function sortByPredefinedOrder($array, $sort)
{
    $sort = array_keys($sort);
    $sort = array_flip($sort);
    foreach ($array as $insd) {
        if (!isset($sort[$insd["name"]])) {
            $sort[$insd["name"]] = 10;
        }
    }
    usort($array, function ($a, $b) use ($sort) {
        return $sort[$a['name']] - $sort[$b['name']];
    });
    return $array;
}

function url_custom($path)
{
    return $path;
}

function bot($api, $chat_id, $message)
{
    $apiToken = $api;
    $reply_markup = [];
    $data = [
        'chat_id' => $chat_id,
        'text' => $message,
        'parse_mode' => 'HTML',
        'reply_markup' => $reply_markup
    ];
    $response = file_get_contents("https://api.telegram.org/bot$apiToken/sendMessage?" . http_build_query($data));
}

function get_type_sql()
{

    $numbers = [
        'boolean' => "boolean",
        'tinyint' => "tinyInteger",
        'smallint' => "smallInteger",
        'mediumint' => "mediumInteger",
        'integer' => "integer",
        'bigint' => "bigInteger",
        'decimal' => "decimal",
        'float' => "float",
        'double' => "double",
    ];

    $strings = [
        'char' => 'char',
        'varchar' => 'string',
        'uuid' => 'uuid',
        'text' => 'text',
        'mediumtext' => 'mediumtext',
        'longtext' => 'longtext',
    ];

    $datetime = [
        'date' => 'date',
        'datetime' => 'datetime',
        'year' => 'smallInteger',
        'time' => 'time',
        'timetz' => 'timeTz',
        'timestamp' => 'timestamp',
        'timestamptz' => 'timestampTz',
        'datetimetz' => 'dateTimeTz',
    ];

    $lists = [
        'enum' => 'enum',
        'json' => 'json',
        'jsonb' => 'jsonb',
    ];

    $binary = [
        'binary' => 'binary',
//        'blob'=>'binary',
    ];

    $network = [
        'macaddr' => 'macAddress',
    ];

//    $geometry = [
//        'geometrycollection'=>'',
//    ];

    return [
        "numbers" => $numbers,
        "strings" => $strings,
        "datetime" => $datetime,
        "lists" => $lists,
        "binary" => $binary,
        "network" => $network,
//        "geometry" => $geometry,
    ];
}
