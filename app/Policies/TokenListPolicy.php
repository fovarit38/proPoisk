<?php

namespace App\Policies;

use App\Models\TokenList;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TokenListPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\TokenList  $tokenList
     * @return mixed
     */
    public function view(User $user, TokenList $tokenList)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\TokenList  $tokenList
     * @return mixed
     */
    public function update(User $user, TokenList $tokenList)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\TokenList  $tokenList
     * @return mixed
     */
    public function delete(User $user, TokenList $tokenList)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\TokenList  $tokenList
     * @return mixed
     */
    public function restore(User $user, TokenList $tokenList)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\TokenList  $tokenList
     * @return mixed
     */
    public function forceDelete(User $user, TokenList $tokenList)
    {
        //
    }
}
